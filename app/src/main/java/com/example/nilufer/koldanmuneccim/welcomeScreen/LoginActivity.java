package com.example.nilufer.koldanmuneccim.welcomeScreen;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.nilufer.koldanmuneccim.MainActivity;
import com.example.nilufer.koldanmuneccim.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {



    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.btnRegister)
    Button btnRegister;


    @BindView(R.id.tvPasswordReset)
    TextView tvPasswordReset;

    @BindView(R.id.ll_login)
    LinearLayout llLogin;

 /*   private ProgressBar mProgressBar;
    private FirebaseAuth mAuth;
    private String TAG1 = "Login";
    private EditText email, password;
    private String emailStr;

    private Animation shake;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        tvPasswordReset.setClickable(true);

        shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        email = findViewById(R.id.etLoginMail);
        password = findViewById(R.id.etPassword);
        mProgressBar = findViewById(R.id.progressBar);
        mAuth = FirebaseAuth.getInstance();

        email.setHintTextColor(Color.parseColor("#9E9E9E"));
        password.setHintTextColor(Color.parseColor("#9E9E9E"));
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

    }


    @OnClick(R.id.btnLogin)
    public void login(View view) {
        emailStr = email.getText().toString().trim();
        String pass = password.getText().toString().trim();
        int flag = 0;

        if(TextUtils.isEmpty(emailStr)){
            email.startAnimation(shake);
            flag = 1;
        }

        if (TextUtils.isEmpty(pass)){
            password.startAnimation(shake);
            flag = 1;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()){
            email.setError(getResources().getString(R.string.enterValidEmail));
            email.startAnimation(shake);
            flag = 1;
        }

        if(pass.length()<6){
            password.setError(getResources().getString(R.string.passwordError_1));
            password.startAnimation(shake);
            flag = 1;
        }

        if (flag == 1)
            return;

        mProgressBar.setVisibility(View.VISIBLE);
        llLogin.setVisibility(View.GONE);
        mAuth.signInWithEmailAndPassword(emailStr, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG1, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                      //      Toast.makeText(LoginActivity.this,"Login Successful!",Toast.LENGTH_SHORT).show();

                            mProgressBar.setVisibility(View.GONE);
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG1, "signInWithEmail:failure", task.getException());
                     /*       Toast.makeText(LoginActivity.this, "Opss. Login failed :(",
                                    Toast.LENGTH_SHORT).show();*/
                      /*      mProgressBar.setVisibility(View.GONE);
                            updateUI(null);
                        }

                        // ...
                    }
                });


        //startActivity(new Intent(this, MainActivity.class));

    }

    private void updateUI(FirebaseUser currentUser){
        if(currentUser != null && currentUser.isEmailVerified()){    // if user already logged in
            // go to homepage
            currentUser.getIdToken(true);
            Intent i = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(i);
            finish();
        }else{
            //

        }
    }

    @OnClick(R.id.btnRegister)
    public void register(View view) {

        startActivity(new Intent(this, RegisterActivity.class));

    }

    @OnClick(R.id.tvPasswordReset)
    public void reset(View view) {

        emailStr = email.getText().toString().trim();

    //    Toast.makeText(getApplicationContext(),"reset", Toast.LENGTH_SHORT).show();

        if (emailStr != null && !emailStr.equals("")){

            mAuth.sendPasswordResetEmail(emailStr); // send reset mail.
            showAlert();
        }else{
            email.startAnimation(shake);
        }


    }

    public void showAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Bilgi");

        // set dialog message
        alertDialogBuilder
                .setMessage("Şifre sıfırlama e-postası gönderilmiştir. Lüften gelen kutunuzu kontrol ediniz.")
                .setCancelable(false)
                .setPositiveButton("TAMAM", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
    */
}
