package com.example.nilufer.koldanmuneccim.retrofit;

import com.example.nilufer.koldanmuneccim.model.MPUDataResponse;
import com.example.nilufer.koldanmuneccim.model.PulseDataResponse;
import com.example.nilufer.koldanmuneccim.model.SendServerDataModel;
import com.example.nilufer.koldanmuneccim.model.ServerRequestModel;
import com.example.nilufer.koldanmuneccim.model.TemperatureDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Nilufer on 15.05.2019.
 */

public interface ServiceCallInterface {

    @POST("GetMpuData")
    Call<List<MPUDataResponse>> getMPUData(@Body ServerRequestModel serverRequestModel);

    @POST("GetTemperatureData")
    Call<List<TemperatureDataResponse>> getTemperatureData(@Body ServerRequestModel serverRequestModel);

    @POST("GetPulseData")
    Call<List<PulseDataResponse>> getPulseData(@Body ServerRequestModel serverRequestModel);

    @POST("SendCountData")
    Call<Void> writeServerData(@Body List<SendServerDataModel> sendServerDataModel);

}
