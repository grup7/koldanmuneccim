package com.example.nilufer.koldanmuneccim.datahistory;

import com.example.nilufer.koldanmuneccim.model.ClassifiedResponse;
import com.example.nilufer.koldanmuneccim.model.MPUDataResponse;
import com.example.nilufer.koldanmuneccim.model.PulseDataResponse;
import com.example.nilufer.koldanmuneccim.model.TemperatureDataResponse;

import java.util.List;

/**
 * Created by Nilufer on 16.05.2019.
 */

public interface ClassifiedDataListener {

    public void takeMpuData(ClassifiedResponse classifiedResponse);
    public void takeTempData(List<TemperatureDataResponse> temperatureDataResponse);
    public void takePulseData(List<PulseDataResponse> pulseDataResponse);

}
