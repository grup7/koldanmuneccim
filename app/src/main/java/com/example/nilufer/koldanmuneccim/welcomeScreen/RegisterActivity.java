package com.example.nilufer.koldanmuneccim.welcomeScreen;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.nilufer.koldanmuneccim.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

 /*   private FirebaseAuth mAuth;
    private String TAG1 = "Account Creation";
    private String TAG2 = "Profile Update";
    private EditText email, password, rePassword;
    private ProgressBar mProgressBar;


    @BindView(R.id.btnRegister)
    Button btnRegister;

    @BindView(R.id.ll_register)
    LinearLayout llRegister;

    private Animation shake;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        ButterKnife.bind(this);
        email = findViewById(R.id.etRegisterMail);
        password = findViewById(R.id.etPasswordRegister);
        rePassword = findViewById(R.id.etPasswordRegisterVerify);
        mProgressBar = findViewById(R.id.progressBar);
        mAuth = FirebaseAuth.getInstance();

        email.setHintTextColor(Color.parseColor("#9E9E9E"));
        password.setHintTextColor(Color.parseColor("#9E9E9E"));
        rePassword.setHintTextColor(Color.parseColor("#9E9E9E"));

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }


    @OnClick(R.id.btnRegister)
    public void register(View view) {
        String mail = email.getText().toString().trim();
        String pass = password.getText().toString().trim();
        String rePass = rePassword.getText().toString().trim();
        int flag = 0;

        if (TextUtils.isEmpty(mail)) {
            email.startAnimation(shake);
            flag = 1;
        }

        if (TextUtils.isEmpty(pass)) {
            password.startAnimation(shake);
            flag = 1;
        }

        if (TextUtils.isEmpty(rePass)) {
            rePassword.startAnimation(shake);
            flag = 1;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
            email.setError(getResources().getString(R.string.enterValidEmail));
            email.startAnimation(shake);
            flag = 1;

        }

        if (pass.length() < 6) {
            password.setError(getResources().getString(R.string.passwordError_1));
            password.startAnimation(shake);
            flag = 1;

        }

        if (!pass.equals(rePass)) {
            rePassword.setError(getResources().getString(R.string.passwordError_2));
            rePassword.startAnimation(shake);
            flag = 1;

        }

        if (flag == 1)
            return;


        mProgressBar.setVisibility(View.VISIBLE);
        llRegister.setVisibility(View.GONE);

        createAccount(mail, pass);
        //startActivity(new Intent(this, LoginActivity.class));

    }


    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {    // if user already logged in
            // go to homepage
            currentUser.getIdToken(true);
            Intent i = new Intent(RegisterActivity.this,LoginActivity.class);
            startActivity(i);
            finish();
        }
    }


    private void createAccount(String mail, String password) {

        mAuth.createUserWithEmailAndPassword(mail, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign up success, update UI with the signed-in user's information
                            Log.d(TAG1, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            if (user != null) {
                                user.sendEmailVerification(); // send verification mail.
                            }
                            mProgressBar.setVisibility(View.GONE);
                        } else {
                            // If sign up fails, display a message to the user.
                            Log.w(TAG1, "createUserWithEmail:failure", task.getException());
              /*              Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show(); */
                  /*          mProgressBar.setVisibility(View.GONE);
                            updateUI(null);
                        }
                        // ...
                    }
                });
    }
*/
}

