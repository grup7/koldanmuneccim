package com.example.nilufer.koldanmuneccim.dashboardscreen;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nilufer.koldanmuneccim.R;
import com.example.nilufer.koldanmuneccim.customview.DashboardItemView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;


public class DashboardScreenFragment extends Fragment implements DashboardScreenContract.View {

    @BindView(R.id.cv_running)
    DashboardItemView runningItem;

    @BindView(R.id.cv_stairs)
    DashboardItemView stairsItem;

    @BindView(R.id.cv_walking)
    DashboardItemView walkingItem;

    @BindView(R.id.cv_pulse)
    DashboardItemView pulseItem;

    @BindView(R.id.cv_sleeping)
    DashboardItemView sleepingItem;

    @BindView(R.id.txt_name_surname)
    TextView nameSurname;

    String username;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.view_dashboard_layout, container, false);
        Bundle args = getArguments();
        ButterKnife.bind(this, view);

        SharedPreferences.Editor editor = getActivity().getSharedPreferences("shared", MODE_PRIVATE).edit();
        editor.putBoolean("isLoginAfter",true);
        editor.apply();

        initView();

        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        username = prefs.getString("name","");

        return view;
    }

    public void initView() {
        initActivities();
    }

    public void initActivities() {
        runningItem.setActivityName("Koşma");
        runningItem.setActivityImage(R.drawable.running);

        stairsItem.setActivityName("Merdiven");
        stairsItem.setActivityImage(R.drawable.stairs);

        walkingItem.setActivityName("Yürüme");
        walkingItem.setActivityImage(R.drawable.footstep);

        pulseItem.setActivityName("Nabız");
        pulseItem.setActivityImage(R.drawable.pulse);

        sleepingItem.setActivityName("Uyku");
        sleepingItem.setActivityImage(R.drawable.sleep);

        if(username!=null)
            nameSurname.setText(username);
        else
            nameSurname.setText("");
    }


}