package com.example.nilufer.koldanmuneccim;

import com.example.nilufer.koldanmuneccim.model.ServerData;

import java.util.List;

/**
 * Created by Nilufer on 24.05.2019.
 */

public interface ServerDataListener {

    void getServerData(List<ServerData> fbResponse);
}
