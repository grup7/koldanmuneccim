package com.example.nilufer.koldanmuneccim.datahistory;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.nilufer.koldanmuneccim.Classifier;
import com.example.nilufer.koldanmuneccim.FbFlagListener;
import com.example.nilufer.koldanmuneccim.OnGetDataListener;
import com.example.nilufer.koldanmuneccim.R;
import com.example.nilufer.koldanmuneccim.model.ClassifiedResponse;
import com.example.nilufer.koldanmuneccim.model.FbResponse;
import com.example.nilufer.koldanmuneccim.model.MPUDataResponse;
import com.example.nilufer.koldanmuneccim.model.PulseDataResponse;
import com.example.nilufer.koldanmuneccim.model.PulseListObj;
import com.example.nilufer.koldanmuneccim.model.ServerData;
import com.example.nilufer.koldanmuneccim.model.ServerRequestModel;
import com.example.nilufer.koldanmuneccim.model.TemperatureDataList;
import com.example.nilufer.koldanmuneccim.model.TemperatureDataResponse;
import com.example.nilufer.koldanmuneccim.model.UserModel;
import com.example.nilufer.koldanmuneccim.retrofit.APIClient;
import com.example.nilufer.koldanmuneccim.retrofit.ServiceCallInterface;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.tensorflow.Server;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Nilufer on 16.05.2019.
 */

public class DataHistory extends Fragment implements ClassifiedDataListener {

    @BindView(R.id.edt_data_date)
    EditText dataDate;

    @BindView(R.id.btn_search)
    Button searchButton;

    @BindView(R.id.run_data_value)
    TextView runDataVal;

    @BindView(R.id.walk_data_value)
    TextView walkDataVal;

    @BindView(R.id.up_stairs_data_value)
    TextView upStairsDataVal;

    @BindView(R.id.down_stairs_data_value)
    TextView downStairsDataVal;

    @BindView(R.id.temperature_data_value)
    TextView temperatureDataVal;

    @BindView(R.id.pulse_data_value)
    TextView pulseDataVal;

    private DatePickerDialog datePickerDialog;
    ServiceCallInterface serviceCallInterface;
    ServerRequestModel serverRequestModel;
    Classifier classifier;
    ClassifiedResponse classifiedResponse;
    DatabaseReference myRef;
    FirebaseDatabase database;
    FbFlagListener fbFlagListener;
    List<Integer> pulseData;
    List<Double> temperatureData;
    String date;
    List<ClassifiedResponse> classifiedResponses;
    Integer walkingData;
    Double runningData;
    Integer upStairsData;
    Integer downStairsData;
    UserModel currentUser;
    ServerData serverDataLast;
    TemperatureDataList temperatureDataList;
    PulseListObj pulseListObj;
    FbResponse fbResponse;
    String newDate;
    ProgressDialog progressDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.history_tab, container, false);
        Bundle args = getArguments();
        ButterKnife.bind(this, view);

        initDatePickerDialog();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        temperatureDataList = new TemperatureDataList();
        pulseListObj = new PulseListObj();

        return view;

    }

    private void initDatePickerDialog() {
        Calendar now = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                            @Override
                                                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                now.set(year, monthOfYear, dayOfMonth);
                                                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                                                String strDate = format.format(now.getTime());
                                                                dataDate.setText(strDate);
                                                            }
                                                        },
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        datePickerDialog.setYearRange(2019, 2019);
        datePickerDialog.setOkText("Tamam");
        datePickerDialog.setAccentColor("#349BC9");
    }

    @OnClick(R.id.edt_data_date)
    public void onClickDate() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @OnClick(R.id.btn_search)
    public void onClickSearch() {
        date = String.valueOf(dataDate.getText());

        takeDataFromDb();
    }


    private void showProgressDialog() {

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Yükleniyor");
        progressDialog.show();

    }


    private void takeDataFromDb() {
        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        String username = prefs.getString("name", "");

        String[] tokens = date.split("-");

        String newDate = tokens[0];

        if (tokens[1].charAt(0) == '0')
            newDate = newDate + "-" + tokens[1].charAt(1);
        else
            newDate = newDate + "-" + tokens[1];

        if (tokens[2].charAt(0) == '0')
            newDate = newDate + "-" + tokens[2].charAt(1);
        else
            newDate = newDate + "-" + tokens[2];

        Query databaseQuery = myRef.child("ServerData").orderByChild("date").equalTo(newDate);
        getData(databaseQuery, new OnGetDataListener() {
            @Override
            public void onStart() {

                showProgressDialog();

            }

            @Override
            public void onSuccess(DataSnapshot data) {
                for (DataSnapshot eachDataSnapShot : data.getChildren())
                    serverDataLast = eachDataSnapShot.getValue(ServerData.class);


                if (serverDataLast == null)
                    serverPostMethods();
                else {
                    progressDialog.dismiss();
                    walkDataVal.setText(serverDataLast.getWalkingData() + "     adım");
                    runDataVal.setText(serverDataLast.getRunningData() + "     m");
                    downStairsDataVal.setText(serverDataLast.getDownStairsData() + "     kat");
                    upStairsDataVal.setText(serverDataLast.getUpStairsData() + "     kat");

                    if (serverDataLast.getPulseListObj() != null && serverDataLast.getPulseListObj().getPulseDataList() != null &&
                            serverDataLast.getPulseListObj().getPulseDataList().get(0) != null)
                        pulseDataVal.setText(serverDataLast.getPulseListObj().getPulseDataList().get(0) + "     bpm");
                    else
                        pulseDataVal.setText("80     bpm");

                    if (serverDataLast.getTemperatureDataList() != null && serverDataLast.getTemperatureDataList().getTemperatureList() != null &&
                            serverDataLast.getTemperatureDataList().getTemperatureList().get(0) != null)
                        temperatureDataVal.setText(serverDataLast.getTemperatureDataList().getTemperatureList().get(0) + "     bpm");
                    else
                        temperatureDataVal.setText("37.2   C°");

                }
            }

            @Override
            public void onFailed(DatabaseError databaseError) {
                serverPostMethods();
            }
        });
    }

    private void getData(Query query, final OnGetDataListener listener) {
        listener.onStart();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);

               /* else {
                    List<ServerData> serverDataList = fbResponse.getServerDataList();
                    ServerData server = null;
                    if (serverDataList != null)
                        server = serverDataList.get(0);
                    if (server != null) {
                        walkDataVal.setText(server.getWalkingData() + "     adım");
                        runDataVal.setText(server.getRunningData() + "     m");
                        downStairsDataVal.setText(server.getDownStairsData() + "     basamak");
                        upStairsDataVal.setText(server.getUpStairsData() + "     basamak");


                        pulseDataVal.setText("80     bpm");

                        temperatureDataVal.setText("37.2   C°");
                    }
                }*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
                Log.d("", "Error trying to get classified ad for update " +
                        "" + databaseError);
            }
        });
    }


    public void serverPostMethods() {
        serviceCallInterface = APIClient.getClient().create(ServiceCallInterface.class);

        String[] tokens = date.split("-");

        newDate = tokens[0];

        if (tokens[1].charAt(0) == '0')
            newDate = newDate + "-" + tokens[1].charAt(1);
        else
            newDate = newDate + "-" + tokens[1];

        if (tokens[2].charAt(0) == '0')
            newDate = newDate + "-" + tokens[2].charAt(1);
        else
            newDate = newDate + "-" + tokens[2];

        serverRequestModel = new ServerRequestModel(13, newDate);
        getMPU();

    }

    public void getMPU() {

        Call<List<MPUDataResponse>> call = serviceCallInterface.getMPUData(serverRequestModel);
        call.enqueue(new Callback<List<MPUDataResponse>>() {
            @Override
            public void onResponse(Call<List<MPUDataResponse>> call, Response<List<MPUDataResponse>> response) {
                List<MPUDataResponse> mpuData = response.body();
                if (mpuData != null && mpuData.size() != 0) {
                    classifier = new Classifier(mpuData, getContext());
                    takeMpuData(classifier.getClassifiedResponse());
                }
            }

            @Override
            public void onFailure(Call<List<MPUDataResponse>> call, Throwable t) {

            }
        });
    }

    public void getTemperature() {

        Call<List<TemperatureDataResponse>> call = serviceCallInterface.getTemperatureData(serverRequestModel);

        call.enqueue(new Callback<List<TemperatureDataResponse>>() {
            @Override
            public void onResponse(Call<List<TemperatureDataResponse>> call, Response<List<TemperatureDataResponse>> response) {
                List<TemperatureDataResponse> temperatureData = response.body();
                takeTempData(temperatureData);
            }

            @Override
            public void onFailure(Call<List<TemperatureDataResponse>> call, Throwable t) {

            }
        });

    }

    public void getPulse() {
        Call<List<PulseDataResponse>> call = serviceCallInterface.getPulseData(serverRequestModel);

        call.enqueue(new Callback<List<PulseDataResponse>>() {
            @Override
            public void onResponse(Call<List<PulseDataResponse>> call, Response<List<PulseDataResponse>> response) {
                List<PulseDataResponse> pulseData = response.body();
                takePulseData(pulseData);

            }

            @Override
            public void onFailure(Call<List<PulseDataResponse>> call, Throwable t) {

            }
        });
    }

    @Override
    public void takeMpuData(ClassifiedResponse classifiedResponse) {
        this.classifiedResponse = classifiedResponse;
        getTemperature();
    }

    @Override
    public void takeTempData(List<TemperatureDataResponse> temperatureDataResponse) {
        Double temperature = 0.0;
        if (temperatureDataResponse.size() != 0) {
            for (int i = 0; i < temperatureDataResponse.size(); ++i) {
                temperature = temperature + temperatureDataResponse.get(i).getTemperature();
                temperatureData.add(temperatureDataResponse.get(i).getTemperature());
            }

            temperature = temperature / temperatureDataResponse.size();

            temperatureDataVal.setText(String.valueOf(temperature));


            temperatureDataList.setTemperatureList(temperatureData);

        }
        getPulse();
    }

    @Override
    public void takePulseData(List<PulseDataResponse> pulseDataResponse) {

        Integer pulse = 0;
        if (pulseDataResponse.size() != 0) {
            for (int i = 0; i < pulseDataResponse.size(); ++i) {
                pulse = pulse + pulseDataResponse.get(i).getPulse();
                pulseData.add(pulseDataResponse.get(i).getPulse());
            }

            pulse = pulse / pulseDataResponse.size();
            pulseDataVal.setText(String.valueOf(pulse));
        }
        pulseListObj = new PulseListObj();
        pulseListObj.setPulseDataList(pulseData);

        fillData();
        saveDataFirebase();
    }

    public void saveDataFirebase() {
        List<Integer> pulse = new ArrayList<>();
        pulse.add(4);
        pulseListObj.setPulseDataList(pulse);

        List<Double> temp = new ArrayList<>();
        temp.add(4.5);
        temperatureDataList.setTemperatureList(temp);


        ServerData serverData = new ServerData(pulseListObj, temperatureDataList, newDate, String.valueOf(walkingData),
                String.valueOf(runningData), String.valueOf(upStairsData), String.valueOf(downStairsData), "6");

        myRef = database.getReference("ServerData");

        myRef.push().setValue(serverData);

    }

    public void fillData() {
        walkingData = classifiedResponse.getWalkingData();
        runningData = classifiedResponse.getRunning();
        downStairsData = classifiedResponse.getDownStairsNumber();
        upStairsData = classifiedResponse.getUpStairsNumber();

        progressDialog.dismiss();

        walkDataVal.setText(classifiedResponse.getWalkingData() + "     adım");
        runDataVal.setText(String.valueOf(classifiedResponse.getRunning()) + "     m");
        downStairsDataVal.setText(classifiedResponse.getDownStairsNumber() + "     kat");
        upStairsDataVal.setText(classifiedResponse.getUpStairsNumber() + "     kat");

        if (pulseData != null && pulseData.size() != 0)
            pulseDataVal.setText(pulseData.get(pulseData.size() - 1) + "     bpm");
        else
            pulseDataVal.setText("80     bpm");

        if (temperatureData != null && temperatureData.size() != 0)
            temperatureDataVal.setText(String.valueOf(temperatureData.get(temperatureData.size() - 1)) + "     C°");
        else
            temperatureDataVal.setText("37.2   C°");

    }
}
