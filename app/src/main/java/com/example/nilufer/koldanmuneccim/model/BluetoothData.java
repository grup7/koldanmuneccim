package com.example.nilufer.koldanmuneccim.model;


public class BluetoothData {
    PulseListObj pulseListObj;
    TemperatureDataList temperatureDataList;
    String date;
    String walkingData;
    String runningData;
    String upStairsData;
    String downStairsData;
    String sleepingData;


    public BluetoothData(PulseListObj pulseListObj, TemperatureDataList temperatureDataList, String date, String walkingData, String runningData, String upStairsData, String downStairsData, String sleepingData) {
        this.pulseListObj = pulseListObj;
        this.temperatureDataList = temperatureDataList;
        this.date = date;
        this.walkingData = walkingData;
        this.runningData = runningData;
        this.upStairsData = upStairsData;
        this.downStairsData = downStairsData;
        this.sleepingData = sleepingData;
    }

    public BluetoothData(){}

    public PulseListObj getPulseListObj() {
        return pulseListObj;
    }

    public void setPulseListObj(PulseListObj pulseListObj) {
        this.pulseListObj = pulseListObj;
    }

    public TemperatureDataList getTemperatureDataList() {
        return temperatureDataList;
    }

    public void setTemperatureDataList(TemperatureDataList temperatureDataList) {
        this.temperatureDataList = temperatureDataList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWalkingData() {
        return walkingData;
    }

    public void setWalkingData(String walkingData) {
        this.walkingData = walkingData;
    }

    public String getRunningData() {
        return runningData;
    }

    public void setRunningData(String runningData) {
        this.runningData = runningData;
    }

    public String getUpStairsData() {
        return upStairsData;
    }

    public void setUpStairsData(String upStairsData) {
        this.upStairsData = upStairsData;
    }

    public String getDownStairsData() {
        return downStairsData;
    }

    public void setDownStairsData(String downStairsData) {
        this.downStairsData = downStairsData;
    }

    public String getSleepingData() {
        return sleepingData;
    }

    public void setSleepingData(String sleepingData) {
        this.sleepingData = sleepingData;
    }
}
