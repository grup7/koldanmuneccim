package com.example.nilufer.koldanmuneccim.profile;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.nilufer.koldanmuneccim.OnGetDataListener;
import com.example.nilufer.koldanmuneccim.R;
import com.example.nilufer.koldanmuneccim.model.UserModel;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    @BindView(R.id.btnEdit)
    Button btnEdit;

    @BindView(R.id.tvHeight)
    TextView tvHeight;

    @BindView(R.id.tvWeight)
    TextView tvWeight;

    @BindView(R.id.tvProfileName)
    TextView tvProfileName;

    @BindView(R.id.ivProfilePhoto)
    CircleImageView ivProfilePhoto;

    @BindView(R.id.walkingTarget)
    TextView walkingTarget;

    @BindView(R.id.runningTarget)
    TextView runningTarget;

    FirebaseDatabase database;
    DatabaseReference myRef;

    ProgressDialog progressDialog;

    UserModel currentUser;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ButterKnife.bind(this, view);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users");

   /*     Bundle bundle = this.getArguments();

        if (bundle != null) {

            String height = bundle.getString("height") + " cm";
            String weight = bundle.getString("weight") + " kg";
            String name = bundle.getString("name");

            tvHeight.setText(height);

            tvWeight.setText(weight);

            tvProfileName.setText(name);
        }*/

        takeDataFromDb();

        showImg();

        // Inflate the layout for this fragment
        return view;
    }

    private void takeDataFromDb() {
        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        String username = prefs.getString("name", "");

        Query databaseQuery = myRef.orderByChild("nameSurname").equalTo(username);
        getData(databaseQuery, new OnGetDataListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(DataSnapshot data) {
                for (DataSnapshot eachDataSnapShot : data.getChildren())
                    currentUser = eachDataSnapShot.getValue(UserModel.class);
                if (currentUser != null) {
                    tvProfileName.setText(currentUser.getNameSurname());
                    tvHeight.setText(currentUser.getHeight()+"     cm");
                    tvWeight.setText(currentUser.getWeight()+"     kg");
                    runningTarget.setText(currentUser.getTargetRunning()+" metre");
                    walkingTarget.setText(currentUser.getTargetWalking() +" adım");
                }
            }

            @Override
            public void onFailed(DatabaseError databaseError) {

            }
        });
    }

    private void getData(Query query, final OnGetDataListener listener) {
        listener.onStart();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);
                     /*   for (DataSnapshot eachDataSnapShot : dataSnapshot.getChildren()) {
                            user = eachDataSnapShot.getValue(UserOldPeople.class);
                        }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
                Log.d("", "Error trying to get classified ad for update " +
                        "" + databaseError);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        //progressDialog.dismiss();
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Yükleniyor");
        progressDialog.show();
    }

    @OnClick(R.id.btnEdit)
    public void edit(View view) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, new EditProfileFragment());
        transaction.commit();
    }

    public void showImg() {
        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        Uri filePath = Uri.parse(prefs.getString("img", ""));

        if (filePath != null) {
            Glide.with(getContext()).load(filePath).asBitmap().centerCrop().into(new SimpleTarget<Bitmap>(200, 200) {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    ivProfilePhoto.setImageBitmap(resource);
                }
            });
        }
    }
}

