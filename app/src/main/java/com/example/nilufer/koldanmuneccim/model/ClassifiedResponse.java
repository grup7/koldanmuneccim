package com.example.nilufer.koldanmuneccim.model;

/**
 * Created by Nilufer on 15.05.2019.
 */

public class ClassifiedResponse {
    int walkingData;
    int upStairsNumber;
    int downStairsNumber;
    double running;

    public ClassifiedResponse(){
        walkingData = 0;
        upStairsNumber = 0;
        downStairsNumber = 0;
        running = 0;
    }

    public int getWalkingData() {
        return walkingData;
    }

    public void setWalkingData(int walkingData) {
        this.walkingData = walkingData;
    }

    public int getUpStairsNumber() {
        return upStairsNumber;
    }

    public void setUpStairsNumber(int upStairsNumber) {
        this.upStairsNumber = upStairsNumber;
    }

    public int getDownStairsNumber() {
        return downStairsNumber;
    }

    public void setDownStairsNumber(int downStairsNumber) {
        this.downStairsNumber = downStairsNumber;
    }

    public double getRunning() {
        return running;
    }

    public void setRunning(double running) {
        this.running = running;
    }
}
