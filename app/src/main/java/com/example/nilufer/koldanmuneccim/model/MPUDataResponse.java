package com.example.nilufer.koldanmuneccim.model;

/**
 * Created by Nilufer on 15.05.2019.
 */

public class MPUDataResponse {
    Double X;
    Double Y;
    Double Z;

    public MPUDataResponse(Double x, Double y, Double z) {
        X = x;
        Y = y;
        Z = z;
    }

    public MPUDataResponse() {
    }

    public Double getX() {
        return X;
    }

    public void setX(Double x) {
        X = x;
    }

    public Double getY() {
        return Y;
    }

    public void setY(Double y) {
        Y = y;
    }

    public Double getZ() {
        return Z;
    }

    public void setZ(Double z) {
        Z = z;
    }
}
