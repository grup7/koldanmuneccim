package com.example.nilufer.koldanmuneccim.model;

/**
 * Created by Nilufer on 15.05.2019.
 */

public class PulseDataResponse {

    Integer pulse;

    public PulseDataResponse(Integer pulse) {
        this.pulse = pulse;
    }

    public Integer getPulse() {
        return pulse;
    }

    public void setPulse(Integer pulse) {
        this.pulse = pulse;
    }
}
