package com.example.nilufer.koldanmuneccim.login;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.nilufer.koldanmuneccim.HomeFragment;
import com.example.nilufer.koldanmuneccim.OnGetDataListener;
import com.example.nilufer.koldanmuneccim.R;
import com.example.nilufer.koldanmuneccim.model.UserModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Nilufer on 9.05.2019.
 */

public class LoginFragment extends Fragment implements TextWatcher {

    @BindView(R.id.btn_login)
    Button loginButton;

    @BindView(R.id.edt_user_phone_login)
    EditText phoneNumber;

    private DatabaseReference dbRef;
    UserModel currentUser;

    public LoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        initView();

        return view;
    }

    public void initView() {
        initTelephoneNumber();
        dbRef = FirebaseDatabase.getInstance().getReference();
        phoneNumber.addTextChangedListener(this);
    }

    private void initTelephoneNumber() {
        TelephonyManager tMgr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String mPhoneNumber = tMgr.getLine1Number();
        String phoneNumberStr = "";
        if (mPhoneNumber != null)
            phoneNumberStr = PhoneNumberUtils.formatNumber(mPhoneNumber, Locale.getDefault().getCountry());
        if (phoneNumberStr != null)
            phoneNumber.setText(phoneNumberStr);
    }

    @OnClick(R.id.btn_login)
    public void onClickLoginButton() {
        isSavedPhone(String.valueOf(phoneNumber.getText()));

    }

    public void controlUser()   {
        if (currentUser != null) {
            SharedPreferences.Editor editor = getActivity().getSharedPreferences("shared", MODE_PRIVATE).edit();
            editor.putString("name",currentUser.getNameSurname());
            editor.putString("phoneNumber", currentUser.getPhoneNumber());
            editor.putString("targetRunning", currentUser.getTargetRunning());
            editor.putString("targetWalking", currentUser.getTargetWalking());
            editor.apply();



            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, new HomeFragment());
            transaction.addToBackStack(HomeFragment.class.getSimpleName());
            transaction.commit();
        }

    }

    private void isSavedPhone(String phone) {
        Query databaseQuery = dbRef.child("Users").orderByChild("phoneNumber").equalTo(phone);
        controlDbToPhone(databaseQuery, new OnGetDataListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(DataSnapshot data) {
                for (DataSnapshot eachDataSnapShot : data.getChildren())
                    currentUser = eachDataSnapShot.getValue(UserModel.class);
                if (currentUser == null) {
                    showAlertDialog("Numaraya ait kullanıcı bulunamadı","Uyarı", "Tamam");
                } else {

                }
                controlUser();
            }

            @Override
            public void onFailed(DatabaseError databaseError) {

            }
        });
    }

    private void showAlertDialog(String message, String title, String positiveText) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                positiveText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void controlDbToPhone(Query query, final OnGetDataListener listener) {
        listener.onStart();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);
                     /*   for (DataSnapshot eachDataSnapShot : dataSnapshot.getChildren()) {
                            user = eachDataSnapShot.getValue(UserOldPeople.class);
                        }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
                Log.d("", "Error trying to get classified ad for update " +
                        "" + databaseError);
            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @OnClick(R.id.fl_login)
    public void onFlLoginClick() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

}
