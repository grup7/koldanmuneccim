package com.example.nilufer.koldanmuneccim.login;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatEditText;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.nilufer.koldanmuneccim.HomeFragment;
import com.example.nilufer.koldanmuneccim.R;
import com.example.nilufer.koldanmuneccim.model.UserModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Nilufer on 9.05.2019.
 */

public class RegisterFragment extends Fragment implements TextWatcher, View.OnFocusChangeListener {

    @BindView(R.id.edt_user_name_surname)
    AppCompatEditText userNameSurname;

    @BindView(R.id.edt_user_phone_number)
    EditText userPhoneNumber;

    @BindView(R.id.edt_user_birthdate)
    EditText userBirthDate;

    @BindView(R.id.gender_choices)
    RadioRealButtonGroup genderToggle;

    @BindView(R.id.weight)
    EditText weight;
    @BindView(R.id.height)
    EditText height;

    String nameSurname;
    String phoneNumber;
    String gender;

    View view;

    DatePickerDialog datePickerDialog;
    FirebaseDatabase database;
    DatabaseReference myRef;
    SharedPreferences.Editor editor;

    public RegisterFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users");

        initView();
        return view;
    }

    public void initView() {

        userNameSurname.addTextChangedListener(this);
        userNameSurname.setOnFocusChangeListener(this);
        initDatePickerDialog();
        initTelephoneNumber();
    }


    private void initDatePickerDialog() {
        Calendar now = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                            @Override
                                                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                now.set(year, monthOfYear, dayOfMonth);
                                                                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                                                                String strDate = format.format(now.getTime());
                                                                userBirthDate.setText(strDate);
                                                            }
                                                        },
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        datePickerDialog.setOkText("Tamam");
        datePickerDialog.setAccentColor("#349BC9");
    }

    private void initTelephoneNumber() {
        TelephonyManager tMgr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String mPhoneNumber = tMgr.getLine1Number();
        userPhoneNumber.setText(mPhoneNumber);
    }

    @OnClick(R.id.btn_register)
    public void onClickRegister() {
        String userName = takeUserNameSurname();
        String phoneNumber = takePhoneNumber();
        String date = takeDate();
        String gender = getGenderInfo();
        String weight = takeWeight();
        String height = takeHeight();
        String targetWalking= "6000";
        String targetRunning = "1200";

        SharedPreferences.Editor editor = getActivity().getSharedPreferences("shared", MODE_PRIVATE).edit();
        editor.putString("phoneNumber",phoneNumber);
        editor.apply();

        if (genderToggle.getPosition() == -1)
            showalert();
        UserModel user = new UserModel(userName, phoneNumber, date, gender, height, weight, targetWalking, targetRunning,"",null);

        if (userName != null && phoneNumber != null && date != null && gender != null && weight != null && height != null)
            writeNewUser(user);

    }


    private void writeNewUser(UserModel user) {

        myRef.push().setValue(user);

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, new HomeFragment());
        transaction.disallowAddToBackStack();
        transaction.commit();

    }

    public void showalert() {
        new AlertDialog.Builder(getActivity())
                .setMessage("Cinsiyet alanını boş bırakamazsınız!")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                    }
                }).show();
    }

    @OnClick(R.id.edt_user_name_surname)
    public void onClickNameSurname() {
        if (userNameSurname.getHint().toString().equals("Ad Soyad"))
            userNameSurname.setHint("");
    }


    @OnClick(R.id.edt_user_phone_number)
    public void onClickPhoneNumber() {
        if (userPhoneNumber.getHint().toString().equals("Cep Telefonu"))
            userPhoneNumber.setHint("");
    }

    @OnClick(R.id.fragment_register)
    public void onClickFragment() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @OnClick(R.id.edt_user_birthdate)
    public void onClickDate() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @OnClick(R.id.height)
    public void onClickHeight() {
        height.setHint("");
    }

    public String takeUserNameSurname() {
        if (userNameSurname.getText().length() == 0) {
            changeUserNameHint(true);
            return "";
        } else
            return String.valueOf(userNameSurname.getText());
    }

    public String takePhoneNumber() {
        if (userPhoneNumber.getText().length() == 0) {
            changePhoneNumberHint(true);
            return "";
        } else
            return String.valueOf(userPhoneNumber.getText());
    }

    public String takeDate() {
        if (userBirthDate.getText().length() == 0) {
            changeDateHint(true);
            return "";
        } else
            return String.valueOf(userBirthDate.getText());
    }

    public String takeWeight() {
        if (weight.getText().length() == 0) {
            changeWeightHint(true);
            return "";
        } else
            return String.valueOf(weight.getText());
    }

    public String takeHeight() {
        if (height.getText().length() == 0) {
            changeHeightHint(true);
            return "";
        } else
            return String.valueOf(height.getText());
    }

    public void changeUserNameHint(boolean isError) {
        if (isError) {
            userNameSurname.setHint("Bu Alanı Boş Bırakamazsınız");
            userNameSurname.getHintTextColors();
            userNameSurname.setHintTextColor(Color.RED);
        } else {
            userNameSurname.setHint("Ad Soyad");
            userNameSurname.setHintTextColor(Color.parseColor("#808080"));

        }
    }

    public void changePhoneNumberHint(boolean isError) {
        if (isError) {
            userPhoneNumber.setHint("Bu Alanı Boş Bırakamazsınız");
            userPhoneNumber.getHintTextColors();
            userPhoneNumber.setHintTextColor(Color.RED);
        } else {
            userPhoneNumber.setHint("Telefon Numarası");
            userPhoneNumber.setHintTextColor(Color.parseColor("#808080"));

        }
    }

    public void changeHeightHint(boolean isError) {
        if (isError) {
            height.setHint("!!!");
            height.getHintTextColors();
            height.setHintTextColor(Color.RED);
        } else {
            height.setHint("Boy");
            height.setHintTextColor(Color.parseColor("#808080"));

        }
    }

    public void changeWeightHint(boolean isError) {
        if (isError) {
            weight.setHint("!!!");
            weight.getHintTextColors();
            weight.setHintTextColor(Color.RED);
        } else {
            weight.setHint("Ağırlık");
            weight.setHintTextColor(Color.parseColor("#808080"));

        }
    }

    public void changeDateHint(boolean isError) {
        if (isError) {
            userBirthDate.setHint("Bu Alanı Boş Bırakamazsınız");
            userBirthDate.getHintTextColors();
            userBirthDate.setHintTextColor(Color.RED);
        } else {
            userBirthDate.setHint("Doğum Tarihi");
            userBirthDate.setHintTextColor(Color.parseColor("#808080"));

        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        Log.i("deneme ", "here");
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (userNameSurname.getText().length() == 0)
            changeUserNameHint(false);
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (userNameSurname.getText().length() == 0)
            changeUserNameHint(false);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        onClickFragment();
        if (userNameSurname.getText().length() == 0)
            changeUserNameHint(false);
        if (userPhoneNumber.getText().length() == 0)
            changePhoneNumberHint(false);
        if (height.getText().length() == 0)
            changeHeightHint(false);
        if (weight.getText().length() == 0)
            changeWeightHint(false);
        if (userBirthDate.getText().length() == 0)
            changeDateHint(false);

    }

    private String getGenderInfo() {
        int position = genderToggle.getPosition();
        if (position == 0)
            return "kadın";
        else if (position == 1)
            return "erkek";
        else
            return null;
    }
}
