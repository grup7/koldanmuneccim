package com.example.nilufer.koldanmuneccim.charts;


import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nilufer.koldanmuneccim.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActivityDetailFragment extends Fragment {

    View view;

    private ViewPagerAdapter mViewPagerAdapter;

    Fragment todayFragment;
    Fragment historyFragment;

    private float indicatorWidth;
    private int deviceWidth;
    private float startPosition, endPosition;
    private Bundle activityDetail;

    @BindView(R.id.tvToday)
    TextView tvToday;

    @BindView(R.id.tvHistory)
    TextView tvHistory;

    @BindView(R.id.myViewPager)
    ViewPager myViewPager;



    private String activityName = null;





    public ActivityDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_activity_detail, container, false);


        Bundle deneme = this.getArguments();

        if (deneme != null) {
            activityName = deneme.getString("activity");


        }
        System.out.println(activityName);


        ButterKnife.bind(this, view);
        initView();

        mViewPagerAdapter = new ViewPagerAdapter(getFragmentManager());


        myViewPager.setAdapter(mViewPagerAdapter);



        setAnimation();
        signInClick();

        myViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()

        {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.i("onPageScrolled", position + " - " + positionOffset + " " + positionOffsetPixels);
            }

            public void onPageSelected(int position) {
                if (position == 0)
                    signInClick();
                else
                    signUpClick();


            }
        });
        return view;
    }

    public void initView() {
        calculateSizes();


        setAnimation();


        todayFragment = new TodayFragment();
        historyFragment = new HistoryFragment();


        if (activityName != null){

            Bundle bundle=new Bundle();
            bundle.putString("activity", activityName);

            todayFragment.setArguments(bundle);
            historyFragment.setArguments(bundle);

        }

    }

    private void calculateSizes() {
        indicatorWidth = getResources().getDimensionPixelSize(R.dimen.tab_indicator_width);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;

        startPosition = deviceWidth / 4 - indicatorWidth / 2;
        endPosition = deviceWidth * 3 / 4 - indicatorWidth / 2;
    }


    public void setAnimation() {
        myViewPager.setPageTransformer(true, new ViewPager.PageTransformer() {
            private static final float MIN_SCALE = 0.85f;
            private static final float MIN_ALPHA = 0.5f;

            public void transformPage(View view, float position) {

                int pageWidth = view.getWidth();
                int pageHeight = view.getHeight();

                if (position < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    view.setAlpha(0f);

                } else if (position <= 1) { // [-1,1]
                    // Modify the default slide transition to shrink the page as well
                    float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                    float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                    float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                    if (position < 0) {
                        view.setTranslationX(horzMargin - vertMargin / 2);
                    } else {
                        view.setTranslationX(-horzMargin + vertMargin / 2);
                    }

                    // Scale the page down (between MIN_SCALE and 1)
                    view.setScaleX(scaleFactor);
                    view.setScaleY(scaleFactor);

                    // Fade the page relative to its size.
                    view.setAlpha(MIN_ALPHA +
                            (scaleFactor - MIN_SCALE) /
                                    (1 - MIN_SCALE) * (1 - MIN_ALPHA));

                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    view.setAlpha(0f);
                }
            }

        });

    }




    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return todayFragment;
            } else if (position == 1) {
                return historyFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @OnClick(R.id.tvToday)
    public void signInClick() {
        myViewPager.setCurrentItem(0);
    }

    @OnClick(R.id.tvHistory)
    public void signUpClick() {
        myViewPager.setCurrentItem(1);
    }


}
