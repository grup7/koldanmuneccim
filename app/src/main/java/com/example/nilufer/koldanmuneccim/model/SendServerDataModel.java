package com.example.nilufer.koldanmuneccim.model;

/**
 * Created by Nilufer on 24.05.2019.
 */

public class SendServerDataModel {
    Integer userid;
    String date;
    Integer type;
    Integer count;

    public SendServerDataModel() {
    }

    public SendServerDataModel(Integer userid, String date, Integer type, Integer count) {
        this.userid = userid;
        this.date = date;
        this.type = type;
        this.count = count;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
