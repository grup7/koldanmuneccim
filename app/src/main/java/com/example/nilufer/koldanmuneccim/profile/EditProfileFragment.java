package com.example.nilufer.koldanmuneccim.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.nilufer.koldanmuneccim.R;
import com.example.nilufer.koldanmuneccim.model.UserModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import com.google.firebase.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.kevalpatel2106.rulerpicker.RulerValuePicker;
import com.kevalpatel2106.rulerpicker.RulerValuePickerListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Nilufer on 19.04.2019.
 */

public class EditProfileFragment extends Fragment {

    @BindView(R.id.rpHeightPicker)
    RulerValuePicker heightPicker;

    @BindView(R.id.rpWeightPicker)
    RulerValuePicker weightPicker;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.edt_user_name_surname)
    EditText etProfileName;

    @BindView(R.id.ivEditProfilePhoto)
    ImageView ivEditProfilePhoto;

    @BindView(R.id.walking_target)
    EditText walkingTarget;

    @BindView(R.id.running_target)
    EditText runningTarget;

    @BindView(R.id.edt_user_birthdate)
    EditText birthdate;

    private static final int IMAGE_REQUEST = 123;
    ProgressDialog progressDialog;
    Integer selectedHeight, selectedWeight;

    private Uri filePath;
    FirebaseDatabase database;
    DatabaseReference myRef;
    FirebaseAuth auth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this, view);

        showImg();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users");
        auth = FirebaseAuth.getInstance();

        heightPicker.selectValue(150);

        heightPicker.setValuePickerListener(new RulerValuePickerListener() {
            @Override
            public void onValueChange(final int selectedValue) {
                selectedHeight = selectedValue;

            }

            @Override
            public void onIntermediateValueChange(final int selectedValue) {

            }
        });

        weightPicker.selectValue(60);

        weightPicker.setValuePickerListener(new RulerValuePickerListener() {
            @Override
            public void onValueChange(final int selectedValue) {
                selectedWeight = selectedValue;
            }

            @Override
            public void onIntermediateValueChange(final int selectedValue) {
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        //  showProfilePhoto();
        //progressDialog.dismiss();
    }

    private void showProgressDialog() {

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Yükleniyor");
        progressDialog.show();

    }

    @OnClick(R.id.ivEditProfilePhoto)
    public void onClickAddPhoto() {
        Intent intent = new Intent();

        intent.setType("image/*");

        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Fotoğraf Seçiniz"), IMAGE_REQUEST);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
            String username = prefs.getString("name", "");
            String file = String.valueOf(filePath);
            Query query = myRef.orderByChild("nameSurname").equalTo(username);
            query.addChildEventListener(new ChildEventListener() {

                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    myRef.child(dataSnapshot.getKey()).child("imgUri").setValue(file);
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            SharedPreferences.Editor editor = getActivity().getSharedPreferences("shared", MODE_PRIVATE).edit();
            editor.putString("img", String.valueOf(filePath));
            editor.apply();
        }
        showImg();
    }

    public void showImg() {

        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        filePath = Uri.parse(prefs.getString("img", ""));

        Glide.with(getContext()).load(filePath).asBitmap().centerCrop().into(new SimpleTarget<Bitmap>(200, 200) {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                ivEditProfilePhoto.setImageBitmap(resource);
            }
        });

    }

    @OnClick(R.id.btn_save)
    public void onClickSave() {
        String nameSurname = String.valueOf(etProfileName.getText());
        String height = String.valueOf(heightPicker.getCurrentValue());
        String weight = String.valueOf(weightPicker.getCurrentValue());
        String date = String.valueOf(birthdate.getText());
        String targetWalking = String.valueOf(walkingTarget.getText());
        String targetRunning = String.valueOf(runningTarget.getText());

        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        String username = prefs.getString("name", "");

        Query query = myRef.orderByChild("nameSurname").equalTo(username);
        query.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                if (nameSurname != null) {
                    myRef.child(dataSnapshot.getKey()).child("nameSurname").setValue(nameSurname);
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("shared", MODE_PRIVATE).edit();
                    editor.putString("name",nameSurname);
                    editor.apply();
                }

                if (height.length() != 0)
                    myRef.child(dataSnapshot.getKey()).child("height").setValue(height);

                if (weight.length() != 0)
                    myRef.child(dataSnapshot.getKey()).child("weight").setValue(weight);

                if (date != null)
                    myRef.child(dataSnapshot.getKey()).child("birthdate").setValue(date);

                if (targetWalking != null)
                    myRef.child(dataSnapshot.getKey()).child("targetWalking").setValue(targetWalking);

                if (targetRunning != null)
                    myRef.child(dataSnapshot.getKey()).child("targetRunning").setValue(targetRunning);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


  /*  private void showProfilePhoto() {
        showProgressDialog();

        firebaseUser = mFirebaseAuthentication.getCurrentUser();
        String childName = "userProfilePhoto";
        if (firebaseUser != null) {
            childName= "userProfilePhoto/" + firebaseUser.getUid();
        }

        StorageReference storageReference = firebaseStorage.getReference().child(childName);

        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {

            @Override
            public void onSuccess(Uri uri) {

                progressDialog.dismiss();


                Glide.with(getContext()).load(uri).asBitmap().centerCrop().into(new SimpleTarget<Bitmap>(200,200) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        ivEditProfilePhoto.setImageBitmap(resource);
                    }
                });

                //Toast.makeText(getContext(), "Fotoğraf Getirildi.", Toast.LENGTH_SHORT).show();

            }

        }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                //Toast.makeText(getContext(), "Fotoğraf Getirilemedi.", Toast.LENGTH_SHORT).show();
            }

        });
    }


    @OnClick(R.id.btnSave)
    public void save(View view) {

        Bundle bundle = new Bundle();
        bundle.putString("height", selectedHeight.toString()); // Put anything what you want

        bundle.putString("weight", selectedWeight.toString());

        bundle.putString("name", etProfileName.getText().toString().trim());

        addUsernameToFirebase(etProfileName.getText().toString().trim());


        Fragment fragment = new ProfileFragment();
        fragment.setArguments(bundle);

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();

    }

    @OnClick(R.id.ivChangePhoto)
    public void change(View view) {

        Fragment fragment = new ChangePhotoFragment();

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();

    }

    public void addUsernameToFirebase(String username){

        if (username.equals("")){
            showDialog("Uyarı","İsim alanı boş bırakılamaz");
        }
        else{
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference myRef = firebaseDatabase.getReference().child("username");
            String notesId = myRef.push().getKey();
            myRef.child(notesId).child("name").setValue(username);
            //showDialog("İşlem Sonucu", "Notunuz başarılı olarak kaydedilmiştir.");
        }
    }

    public void showDialog(final String title, final String message){

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("TAMAM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }
*/

}
