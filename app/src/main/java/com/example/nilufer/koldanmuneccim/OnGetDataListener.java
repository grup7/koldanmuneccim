package com.example.nilufer.koldanmuneccim;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

/**
 * Created by Nilufer on 14.05.2019.
 */

public interface OnGetDataListener {
    public void onStart();

    public void onSuccess(DataSnapshot data);

    public void onFailed(DatabaseError databaseError);
}

