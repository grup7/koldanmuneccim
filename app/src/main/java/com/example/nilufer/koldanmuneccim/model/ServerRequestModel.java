package com.example.nilufer.koldanmuneccim.model;

/**
 * Created by Nilufer on 14.05.2019.
 */

public class ServerRequestModel {
    private Integer userId;
    private String date;

    public ServerRequestModel(Integer userId, String date) {
        this.userId = userId;
        this.date = date;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
