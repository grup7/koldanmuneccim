package com.example.nilufer.koldanmuneccim.dashboardscreen;

/**
 * Created by Nilufer on 15.03.2019.
 */

public interface DashboardScreenContract {
    interface View {
        //view operations
    }

    interface Presenter    {
        //Service call methods
    }
}
