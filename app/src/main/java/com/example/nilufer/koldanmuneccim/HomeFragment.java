package com.example.nilufer.koldanmuneccim;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.nilufer.koldanmuneccim.bluetooth.ConnectToBluetoothFragment;
import com.example.nilufer.koldanmuneccim.charts.ActivityDetailFragment;
import com.example.nilufer.koldanmuneccim.customview.DashboardItemView;
import com.example.nilufer.koldanmuneccim.datahistory.DataHistory;
import com.example.nilufer.koldanmuneccim.model.BluetoothData;
import com.example.nilufer.koldanmuneccim.profile.ProfileFragment;
import com.example.nilufer.koldanmuneccim.wifi.ConnectWiFi;
import com.example.nilufer.koldanmuneccim.writeToUs.WriteToUsFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.infideap.drawerbehavior.Advance3DDrawerLayout;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;


public class HomeFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener {

    public static Fragment fragment;

    ActionBarDrawerToggle actionBarDrawerToggle;
    Advance3DDrawerLayout drawer;
    View view;

    @BindView(R.id.cv_running)
    DashboardItemView runningItem;

    @BindView(R.id.cv_stairs)
    DashboardItemView stairsItem;

    @BindView(R.id.cv_walking)
    DashboardItemView walkingItem;

    @BindView(R.id.cv_pulse)
    DashboardItemView pulseItem;

    @BindView(R.id.cv_sleeping)
    DashboardItemView sleepingItem;

    @BindView(R.id.txt_name_surname)
    TextView nameSurname;

    @BindView(R.id.img_activity_user_profil)
    CircleImageView userProfilPhoto;

    String username;
    DatabaseReference myRef;
    FirebaseDatabase database;
    private static final int REQUEST_ENABLE_BT = 0;
    private static final int REQUEST_DISCOVER_BT = 1;
    boolean btStatus = false;


    int stepCount = 0;
    int stairCount = 0;
    double runCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);

        drawer = view.findViewById(R.id.drawer_layout);

        Toolbar toolbar = view.findViewById(R.id.toolbar);

        actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView navigationView = view.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        drawer.useCustomBehavior(Gravity.START); //assign custom behavior for "Left" drawer
        drawer.setViewRotation(Gravity.START, 15);
        drawer.setViewScale(Gravity.START, 0.9f); //set height scale for main view (0f to 1f)
        drawer.setViewElevation(Gravity.START, 20); //set main view elevation when drawer open (dimension)
        drawer.setViewScrimColor(Gravity.START, Color.TRANSPARENT); //set drawer overlay coloe (color)
        drawer.setDrawerElevation(Gravity.START, 20); //set drawer elevation (dimension)
        drawer.setContrastThreshold(3); //set maximum of contrast ratio between white text and background color.
        drawer.setRadius(Gravity.START, 25); //set end container's corner radius (dimension)

        SharedPreferences.Editor editor = getActivity().getSharedPreferences("shared", MODE_PRIVATE).edit();
        editor.putBoolean("isLoginAfter", true);
        editor.apply();

        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        username = prefs.getString("name", "");
        showImg();
        initView();

        return view;

    }

    public void initView() {
        initActivities();
    }

    public void initActivities() {
        runningItem.setActivityName("Koşma");
        runningItem.setActivityImage(R.drawable.running);

        DatabaseReference myRef4 = FirebaseDatabase.getInstance().getReference().child("BluetoothData").child("Running");
        myRef4.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null)
                    runningItem.setActivityName(dataSnapshot.getValue(String.class) + " Metre");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        stairsItem.setActivityName("Merdiven");
        stairsItem.setActivityImage(R.drawable.stairs);

        DatabaseReference myRef3 = FirebaseDatabase.getInstance().getReference().child("BluetoothData").child("Stairs");
        myRef3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null)
                    stairsItem.setActivityName(dataSnapshot.getValue(String.class) + " Kat");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        walkingItem.setActivityName("Yürüme");
        walkingItem.setActivityImage(R.drawable.footstep);

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference().child("BluetoothData").child("deneme");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null)
                    walkingItem.setActivityName(dataSnapshot.getValue(String.class) + " Adım");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        pulseItem.setActivityName("Nabız");
        pulseItem.setActivityImage(R.drawable.pulse);

        DatabaseReference myRef1 = FirebaseDatabase.getInstance().getReference().child("BluetoothData").child("Pulse");
        myRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null)
                    pulseItem.setActivityName(dataSnapshot.getValue(String.class) + " BPM");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        sleepingItem.setActivityName("Sıcaklık");
        sleepingItem.setActivityImage(R.drawable.thermometer);

        DatabaseReference myRef2 = FirebaseDatabase.getInstance().getReference().child("BluetoothData").child("Temperature");
        myRef2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null)
                    sleepingItem.setActivityName(dataSnapshot.getValue(String.class) + " °C");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        if (username != null)
            nameSurname.setText(username);
        else
            nameSurname.setText("");
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment selectedFragment = null;

        switch (id) {
            case R.id.nav_Home:
                selectedFragment = new HomeFragment();
                break;

            case R.id.nav_profile:
                selectedFragment = new ProfileFragment();
                break;

            case R.id.nav_about_us:
                selectedFragment = new WriteToUsFragment();
                break;

       /*     case R.id.nav_connect_to_watch:
                selectedFragment = new ConnectToBluetoothFragment();
                break;
*/
            case R.id.nav_connect_to_WiFi:
                selectedFragment = new ConnectWiFi();
                break;

            case R.id.nav_history:
                selectedFragment = new DataHistory();
                break;

            case R.id.nav_logout:
                createDialog(getContext());
                break;
        }

        if (selectedFragment != null) {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, selectedFragment);
            transaction.addToBackStack(ConnectToBluetoothFragment.class.getSimpleName());
            transaction.commit();
        }


        DrawerLayout drawer = (DrawerLayout) view.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void createDialog(final Context context) {
        new FancyGifDialog.Builder(getActivity())
                .setMessage("Uygulamadan çıkış yapmak istediğinize emin misiniz?")
                .setNegativeBtnText("Vazgeç")
                .setPositiveBtnBackground("#000000")
                .setPositiveBtnText("Çıkış Yap")
                .setNegativeBtnBackground("#000000")
                .setGifResource(R.drawable.giphy)   //Pass your Gif here
                .isCancellable(true)
                .OnPositiveClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                        getActivity().finish();
                    }
                })
                .OnNegativeClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }

    @OnClick(R.id.cv_running)
    public void runningClick(View v) {

        beginTransaction("Running");
    }

    @OnClick(R.id.cv_stairs)
    public void stairsClick(View v) {

        beginTransaction("Stairs");
    }

    @OnClick(R.id.cv_walking)
    public void walkingClick(View v) {

        beginTransaction("Walking");
    }

    @OnClick(R.id.cv_pulse)
    public void pulseClick(View v) {

        beginTransaction("Pulse");
    }

    @OnClick(R.id.cv_sleeping)
    public void sleepingClick(View v) {

        beginTransaction("Temperature");
    }


    private void beginTransaction(String activityName) {

        Bundle bundle = new Bundle();
        bundle.putString("activity", activityName);

        Fragment fragment = new ActivityDetailFragment();

        fragment.setArguments(bundle);

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(ConnectToBluetoothFragment.class.getSimpleName());
        transaction.commit();
    }

    public void showImg() {

        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        Uri filePath = Uri.parse(prefs.getString("img", ""));

        if (filePath == null) {
            takeImgFromFirebase();
        }

        if (filePath != null) {
            Glide.with(getContext()).load(filePath).asBitmap().centerCrop().into(new SimpleTarget<Bitmap>(200, 200) {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    userProfilPhoto.setImageBitmap(resource);
                }
            });
        }
    }

    public void takeImgFromFirebase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = database.getReference("Users");
        dbRef.child("imgUri").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String file = "";
                        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            file = (String) singleSnapshot.getValue();
                        }
                        SharedPreferences.Editor editor = getActivity().getSharedPreferences("shared", MODE_PRIVATE).edit();
                        editor.putString("img", String.valueOf(file));
                        editor.apply();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, "Error trying to get classified ad for update " +
                                "" + databaseError);
                    }
                });

    }

}
