package com.example.nilufer.koldanmuneccim.bluetooth;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.example.nilufer.koldanmuneccim.Classifier;
import com.example.nilufer.koldanmuneccim.R;
import com.example.nilufer.koldanmuneccim.model.ClassifiedResponse;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConnectToBluetoothFragment extends Fragment {
    private final int REQUEST_ENABLE_BT = 0;
    DatabaseReference myRef;
    FirebaseDatabase database;
    public Classifier classifier;

    @BindView(R.id.animation_view)
    LottieAnimationView lottieAnimationView;
    String fragmentName;

    private Switch aSwitch;
    private TextView label;

    boolean btStatus = false;
    String bluetoothstate = "Bluetooth Durumu: ";
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    boolean connectionStatus = false;

    public ConnectToBluetoothFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connect_to_bluetooth, container, false);
        ButterKnife.bind(this, view);
        aSwitch = view.findViewById(R.id.switch1);
        label = view.findViewById(R.id.label);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        classifier = new Classifier(getContext());

        /*
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        checkBluetooth();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        getActivity().registerReceiver(receiver, filter);
        */
        return view;
    }

    public void animate(View v) {
        if (lottieAnimationView.isAnimating()) {
            lottieAnimationView.cancelAnimation();
        } else {
            lottieAnimationView.playAnimation();
        }
    }

    public void checkBluetooth() {
        if (mBluetoothAdapter == null) {
            Toast.makeText(ConnectToBluetoothFragment.this.getContext(), "This device doesn't have a bluetooth", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!mBluetoothAdapter.isEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_ENABLE_BT);
        }else{
            aSwitch.setChecked(true);
            btStatus = true;
            // bt enabled check for connection
            checkForConnection();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK) {
                  //  lottieAnimationView.setVisibility(View.VISIBLE); //helloooo
                    aSwitch.setText(bluetoothstate+"Etkin");
                    aSwitch.setChecked(true);
                    btStatus = true;
                    checkForConnection();
                } 
                if (resultCode == RESULT_CANCELED) {
                    aSwitch.setText(bluetoothstate+"Etkin değil");
                    btStatus = false;
                }

        }
    }


    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            //Log.d(TAG, "onReceive: ACTION FOUND.");
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
            BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    //setButtonText("Bluetooth off");
                    Log.d("BTSTATE", "Bt Off");
                    System.out.println("BT State_off");

                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    //setButtonText("Turning Bluetooth off...");
                    System.out.println("BT State_Turning_off");
                    break;
                case BluetoothAdapter.STATE_ON:
                    //setButtonText("Bluetooth on");
                    System.out.println("BT State_on");
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    //setButtonText("Turning Bluetooth on...");
                    System.out.println("BT State_Turning_on");
                    break;
            }

            if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                connectionStatus = false;
                System.out.println("BT Connection_disconnected");
            }


        }
    };

    @TargetApi(23)
    private void checkBTPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck;
            permissionCheck = this.getActivity().checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION") + this.getActivity().checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        } else {
            //Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    public boolean createBond(BluetoothDevice btDevice) throws Exception {
        Class class1 = Class.forName("android.bluetooth.BluetoothDevice");
        Method createBondMethod = class1.getMethod("createBond");
        Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }



    public void showAlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle("Caution");
        alertDialog.setMessage("Lütfen cihazınızın bluetooht ayarlarından 'grup7' cihazı ile eşleşin");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Tamam",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    void beginListenForData() {
        System.out.println("Data listening");
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable() {
            public void run() {
                while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                    try {

                        int bytesAvailable = mmInputStream.available();
                        if (bytesAvailable > 0) {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for (int i = 0; i < bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                if (b == delimiter) {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable() {
                                        public void run() {
                                            try {
                                                Log.d("BTREADER", "" + "Hello");
                                                System.out.println("Data recieved");
                                                JSONObject reader = new JSONObject(data);
                                                parseJSON(reader);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                } else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    } catch (IOException ex) {
                        stopWorker = true;
                        System.out.println("worker stopped");
                        Log.d("BTSTATE", "Stoped");
                        connectionStatus = false;
                    }

                }
            }
        });

        workerThread.start();
    }

    public void parseJSON(JSONObject jsonObject) {
        if (jsonObject == null)
            return;
        try {
            int actionType;
            String date = "";
            actionType = jsonObject.getInt("A");
            date = jsonObject.getString("D");
            switch (actionType) {
                case 1:
                    float x, y, z;
                    x = ((float) jsonObject.getDouble("X"));
                    y = ((float) jsonObject.getDouble("Y"));
                    z = ((float) jsonObject.getDouble("Z"));
                    Log.i("",String.valueOf(x));
                    classifier.addValues(x, y, z);
                    classifier.activityPrediction();
                    ClassifiedResponse cR = classifier.getClassifiedResponse();

                    if(cR!=null) {
                        cR.getDownStairsNumber();
                        cR.getUpStairsNumber();
                        cR.getRunning();
                        DatabaseReference dbRef = database.getReference("BluetoothData").child("deneme");
                        dbRef.setValue(String.valueOf(cR.getWalkingData()));
                        System.out.println("Step counter:"+cR.getWalkingData());
                    }

                    break;
                case 2:
                    //MainActivity.pulse.add(jsonObject.getInt("P"));        // gerekli yerde gösterilecek
                    Log.i("",String.valueOf(jsonObject.getInt("P")));
                    break;
                case 3:
                    //MainActivity.temperature.add(jsonObject.getDouble("T"));    // gerekli yerde gösterilecek
                    Log.i("",String.valueOf(jsonObject.getInt("T")));
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }





    ////////////////////////////////////////////////////////////////////////////////////////////
    boolean findBT() {
        System.out.println("Finding bt");
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        boolean found = false;
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equalsIgnoreCase("grup7")) {
                    found = true;
                    mmDevice = device;
                    break;
                }
            }
        }
        if(found)
            return true;
        else{
            showAlertDialog();
            return false;
        }
    }

    void openBT() throws IOException {
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);

        if (!mmSocket.isConnected()) {
            mmSocket.connect();
            System.out.println("Yeni bağlantı kuruluyor");
        }
        else{
            System.out.println("Zaten bağlantı var");
        }
        mmOutputStream = mmSocket.getOutputStream();
        mmInputStream = mmSocket.getInputStream();


        label.setText("Bluetooth Bağlantısı Kuruldu");
        beginListenForData();
    }

    public void closeBT() throws IOException {

        aSwitch.setChecked(false);      // switch will be off
        stopWorker = true;
        mmOutputStream.close();
        mmInputStream.close();
        mmSocket.close();
        mmDevice = null;
        btStatus = false;
        connectionStatus = false;

        label.setText("Bluetooth Kapatıldı");    // switch text will be change

    }

    void checkForConnection(){
        try {
            boolean b = findBT();
            if(b == true)
            openBT();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Connection check finished.");
    }


}
