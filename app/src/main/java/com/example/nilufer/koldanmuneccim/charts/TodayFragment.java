package com.example.nilufer.koldanmuneccim.charts;


import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nilufer.koldanmuneccim.Classifier;
import com.example.nilufer.koldanmuneccim.MainActivity;
import com.example.nilufer.koldanmuneccim.R;
import com.example.nilufer.koldanmuneccim.model.ClassifiedResponse;
import com.example.nilufer.koldanmuneccim.model.PulseListObj;
import com.example.nilufer.koldanmuneccim.model.ServerData;
import com.example.nilufer.koldanmuneccim.model.ServerRequestModel;
import com.example.nilufer.koldanmuneccim.model.TemperatureDataList;
import com.example.nilufer.koldanmuneccim.model.UserModel;
import com.example.nilufer.koldanmuneccim.retrofit.ServiceCallInterface;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import devlight.io.library.ArcProgressStackView;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodayFragment extends Fragment {

    View view;


    @BindView(R.id.heartBeat)
    ImageView heartbeat;

    @BindView(R.id.llStairGraphics)
    LinearLayout llStairGraphics;

    @BindView(R.id.llHeartBeat)
    LinearLayout llHeartBeat;

    @BindView(R.id.llTemperature)
    LinearLayout llTemperature;

    @BindView(R.id.apsv)
    ArcProgressStackView arcProgressStackView;

    @BindView(R.id.ivGraphicInside)
    ImageView ivGraphicInside;

    @BindView(R.id.tvHeartBeatRPM)
    TextView tvHeartBeatRPM;

    @BindView(R.id.tvTemperature)
    TextView tvTemperature;

    @BindView(R.id.tvStepCounter)
    TextView tvStepCounter;

    private String activityName = "";
    private int targetWalkingDb, targetRunningDb;

    ServiceCallInterface serviceCallInterface;
    ServerRequestModel serverRequestModel;
    Classifier classifier;
    ClassifiedResponse classifiedResponse;
    DatabaseReference myRef;
    FirebaseDatabase database;
    List<Integer> pulseData;
    List<Double> temperatureData;
    String date;
    List<ClassifiedResponse> classifiedResponses;
    Integer walkingData;
    Double runningData;
    Integer upStairsData;
    Integer downStairsData;
    UserModel currentUser;
    ServerData serverDataLast;
    TemperatureDataList temperatureDataList;
    PulseListObj pulseListObj;

    public TodayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_today, container, false);

        ButterKnife.bind(this, view);

        Bundle deneme = this.getArguments();

        if (deneme != null) {

            activityName = deneme.getString("activity");

        }


        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        String targetRunning = prefs.getString("targetRunning", null);
        String targetWalking = prefs.getString("targetRunning", null);

        if (targetRunning != null && targetWalking != null){

            if(targetRunning.equalsIgnoreCase("") || targetWalking.equalsIgnoreCase("")){

            }else{
            targetRunningDb = Integer.valueOf(targetRunning);
            targetWalkingDb = Integer.valueOf(targetWalking);
        }}




        if (activityName != null && activityName.equals("Stairs")) {

            //ivGraphicInside.setImageDrawable(getResources().getDrawable(R.drawable.stairs));


            arcProgressStackView.setVisibility(View.GONE);
            llHeartBeat.setVisibility(View.GONE);
            llStairGraphics.setVisibility(View.VISIBLE);
            ivGraphicInside.setVisibility(View.GONE);
            llTemperature.setVisibility(View.GONE);

            tvStepCounter.setText("1 Kat");

        }

        else if(activityName != null && activityName.equals("Pulse")){

            ivGraphicInside.setVisibility(View.GONE);
            llStairGraphics.setVisibility(View.GONE);
            arcProgressStackView.setVisibility(View.GONE);
            llHeartBeat.setVisibility(View.VISIBLE);
            llTemperature.setVisibility(View.GONE);

            tvHeartBeatRPM.setText("100 BPM");

            ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                    heartbeat,
                    PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                    PropertyValuesHolder.ofFloat("scaleY", 1.2f));
            scaleDown.setDuration(500);

            scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
            scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

            scaleDown.start();

        }

        else if (activityName != null && activityName.equals("Temperature")){

            ivGraphicInside.setVisibility(View.GONE);
            llStairGraphics.setVisibility(View.GONE);
            arcProgressStackView.setVisibility(View.GONE);
            llHeartBeat.setVisibility(View.GONE);
            llTemperature.setVisibility(View.VISIBLE);

            tvTemperature.setText("36.5 C");


        }
        else {



            arcProgressStackView.setVisibility(View.VISIBLE);
            llStairGraphics.setVisibility(View.GONE);
            llHeartBeat.setVisibility(View.GONE);
            llTemperature.setVisibility(View.GONE);

            int percentage = 25;

            switch (activityName) {
                case "Walking":
                    activityName = "1200 Adım";
                    percentage = (100*240) / targetWalkingDb;
                    ivGraphicInside.setImageDrawable(getResources().getDrawable(R.drawable.footstep));
                    break;
                case "Running":
                    percentage = (100*200) / targetRunningDb;
                    activityName = "5000 m";
                    break;
            }

            final ArrayList<ArcProgressStackView.Model> models = new ArrayList<>();
            models.add(new ArcProgressStackView.Model(activityName, percentage, getResources().getColor(R.color.deneme), getResources().getColor(R.color.generalBlue)));

            arcProgressStackView.setModels(models);

        }

        //takeDataFromDb();

        return view;
    }

/*
    private void takeDataFromDb() {
        SharedPreferences prefs = getActivity().getSharedPreferences("shared", MODE_PRIVATE);
        String username = prefs.getString("name", "");


        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        Query databaseQuery = myRef.orderByChild("date").equalTo("2019-5-16");
        getData(databaseQuery, new OnGetDataListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(DataSnapshot data) {
                for (DataSnapshot eachDataSnapShot : data.getChildren())
                    serverDataLast = eachDataSnapShot.getValue(ServerData.class);
                if (serverDataLast != null) {

                }
            }

            @Override
            public void onFailed(DatabaseError databaseError) {

            }
        });
    }

    private void getData(Query query, final OnGetDataListener listener) {
        listener.onStart();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);
                for (DataSnapshot eachDataSnapShot : dataSnapshot.getChildren()) {
                    serverDataLast = eachDataSnapShot.getValue(ServerData.class);
                }

                if (serverDataLast == null)
                    serverPostMethods();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
                Log.d("", "Error trying to get classified ad for update " +
                        "" + databaseError);
            }
        });
    }


    public void serverPostMethods() {
        serviceCallInterface = APIClient.getClient().create(ServiceCallInterface.class);
        serverRequestModel = new ServerRequestModel(13, date);
        getMPU();

    }

    public void getMPU() {
        Call<List<MPUDataResponse>> call = serviceCallInterface.getMPUData(serverRequestModel);

        call.enqueue(new Callback<List<MPUDataResponse>>() {
            @Override
            public void onResponse(Call<List<MPUDataResponse>> call, Response<List<MPUDataResponse>> response) {
                List<MPUDataResponse> mpuData = response.body();
                if (mpuData != null && mpuData.size() != 0) {
                    classifier = new Classifier(mpuData);
                    takeMpuData(classifier.getClassifiedResponse());
                }
            }

            @Override
            public void onFailure(Call<List<MPUDataResponse>> call, Throwable t) {

            }
        });
    }

    public void getTemperature() {

        Call<List<TemperatureDataResponse>> call = serviceCallInterface.getTemperatureData(serverRequestModel);

        call.enqueue(new Callback<List<TemperatureDataResponse>>() {
            @Override
            public void onResponse(Call<List<TemperatureDataResponse>> call, Response<List<TemperatureDataResponse>> response) {
                List<TemperatureDataResponse> temperatureData = response.body();
                takeTempData(temperatureData);
            }

            @Override
            public void onFailure(Call<List<TemperatureDataResponse>> call, Throwable t) {

            }
        });

    }

    public void getPulse() {
        Call<List<PulseDataResponse>> call = serviceCallInterface.getPulseData(serverRequestModel);

        call.enqueue(new Callback<List<PulseDataResponse>>() {
            @Override
            public void onResponse(Call<List<PulseDataResponse>> call, Response<List<PulseDataResponse>> response) {
                List<PulseDataResponse> pulseData = response.body();
                takePulseData(pulseData);

            }

            @Override
            public void onFailure(Call<List<PulseDataResponse>> call, Throwable t) {

            }
        });
    }

    @Override
    public void takeMpuData(ClassifiedResponse classifiedResponse) {
        this.classifiedResponse = classifiedResponse;
        fillData();
        getTemperature();
    }

    @Override
    public void takeTempData(List<TemperatureDataResponse> temperatureDataResponse) {
        Double temperature = 0.0;
        if (temperatureDataResponse.size() != 0) {
            for (int i = 0; i < temperatureDataResponse.size(); ++i) {
                temperature = temperature + temperatureDataResponse.get(i).getTemperature();
                temperatureData.add(temperatureDataResponse.get(i).getTemperature());
            }

            temperature = temperature / temperatureDataResponse.size();

            temperatureDataList.setTemperatureList(temperatureData);

        }
        getPulse();
    }

    @Override
    public void takePulseData(List<PulseDataResponse> pulseDataResponse) {

        Integer pulse = 0;
        if (pulseDataResponse.size() != 0) {
            for (int i = 0; i < pulseDataResponse.size(); ++i) {
                pulse = pulse + pulseDataResponse.get(i).getPulse();
                pulseData.add(pulseDataResponse.get(i).getPulse());
            }

            pulse = pulse / pulseDataResponse.size();
        }
        pulseListObj = new PulseListObj();
        pulseListObj.setPulseDataList(pulseData);

        saveDataFirebase();
    }

    public void saveDataFirebase() {
        List<Integer> pulse= new ArrayList<>();
        pulse.add(4);
        pulseListObj.setPulseDataList(pulse);


        ServerData serverData = new ServerData(pulseListObj, temperatureDataList, date, String.valueOf(walkingData),
                String.valueOf(runningData), String.valueOf(upStairsData), String.valueOf(downStairsData), "6");

        myRef = database.getReference("ServerData");

        myRef.push().setValue(serverData);

    }

    public void fillData() {
        walkingData = classifiedResponse.getWalkingData();
        runningData = classifiedResponse.getRunning();
        downStairsData = classifiedResponse.getDownStairsNumber();
        upStairsData = classifiedResponse.getUpStairsNumber();

        Log.d("TAG", walkingData + "/" + runningData+"/"+downStairsData+"/"+upStairsData);

    }*/


}
