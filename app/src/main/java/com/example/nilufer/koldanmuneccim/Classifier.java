package com.example.nilufer.koldanmuneccim;

import android.content.Context;

import com.example.nilufer.koldanmuneccim.model.ClassifiedResponse;
import com.example.nilufer.koldanmuneccim.model.MPUDataResponse;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.log;
import static java.lang.Math.sin;

/**
 * Created by Nilufer on 15.05.2019.
 */

public class Classifier {


    private static final int N_SAMPLES = 32;
    private static List<Float> x = new ArrayList<>();
    private static List<Float> y = new ArrayList<>();
    private static List<Float> z = new ArrayList<>();

    Context mcontext;

    private float[] results;
    private TensorFlowClassifier classifier;
    private int step = 0;
    private int upstairs = 0;
    private int downstairs = 0;
    private List<MPUDataResponse> mpuDataResponse;
    String[] labels = {"Downstairs", "Running", "Sitting", "Upstairs", "Walking"};

    int actionType;
    String date;
    long t0 ;
    double total_running_time;
    private ClassifiedResponse classifiedResponse;

    public Classifier(List<MPUDataResponse> mpuDataResponse, Context context) {
        String[] labels = {"Downstairs", "Running", "Sitting", "Upstairs", "Walking"};

        actionType = 0;             // 1:Mpu 2:Temperature 3:Pulse
        date ="";
        mcontext = context;
        t0 = 0;
        total_running_time = 0;
        this.mpuDataResponse = mpuDataResponse;
        classifier = new TensorFlowClassifier(mcontext);

        classifiedResponse = new ClassifiedResponse();

        callActivityPrediction();

        classifiedResponse.setWalkingData(step);
        classifiedResponse.setDownStairsNumber(downstairs);
        classifiedResponse.setRunning(total_running_time*2.22);
        classifiedResponse.setUpStairsNumber(upstairs);
    }

    public Classifier(){}

    public Classifier(Context mcontext) {
        this.mcontext = mcontext;
        classifier = new TensorFlowClassifier(mcontext);
        classifiedResponse = new ClassifiedResponse();

    }

    public void callActivityPrediction() {

        for(int i=0;i<mpuDataResponse.size();++i)   {
            double temp = mpuDataResponse.get(i).getX();
            Float value = (float) temp;
            x.add(value);

            double temp2 = mpuDataResponse.get(i).getY();
            value = (float) temp2;
            y.add(value);

            double temp3 = mpuDataResponse.get(i).getZ();
            value = (float) temp3;
            z.add(value);

            activityPrediction();
        }
    }


    public void activityPrediction() {

        if (x.size() == N_SAMPLES && y.size() == N_SAMPLES && z.size() == N_SAMPLES) {

            List<Float> data = new ArrayList<>();
            data.addAll(x);
            data.addAll(y);
            data.addAll(z);

            results = classifier.predictProbabilities(toFloatArray(data));

            String activityName = findMax(results);
            double frequency = 0;
            if(activityName.equalsIgnoreCase("walking")){
                frequency = findFrequency(x);
                step= step + (int)(2*frequency);
            }
            if(activityName.equalsIgnoreCase("running")){
                //long t1 = System.currentTimeMillis();
                //long Tdifference = t1 - t0;               // 32 lik datanın gelme süresi ölçüldüğünde bu sabit olacak
                double Tdifference = 1.6;                   // şuanlık saniyede 20 data geldiğini varsayıyorum
                total_running_time = total_running_time + Tdifference;
            }
            if(activityName.equalsIgnoreCase("upstairs")){
                frequency = findFrequency(z);
                upstairs = upstairs + (int)(frequency);
            }
            if(activityName.equalsIgnoreCase("downstairs")){
                frequency = findFrequency(x);
                downstairs = step + (int)(frequency);
            }

            x.clear();
            y.clear();
            z.clear();

            classifiedResponse.setWalkingData(step);
            classifiedResponse.setDownStairsNumber(downstairs);
            classifiedResponse.setRunning(total_running_time*2.22);
            classifiedResponse.setUpStairsNumber(upstairs);

        }else {

        }
    }

    private float[] toFloatArray(List<Float> list) {
        int i = 0;
        float[] array = new float[list.size()];

        for (Float f : list) {
            array[i++] = (f != null ? f : Float.NaN);
        }
        return array;
    }

    String findMax(float[] array){
        String result = "";
        int maxIndex = 0;
        for (int i = 1; i<array.length;i++){
            if(array[i]>array[maxIndex]){
                maxIndex = i;
            }
        }
        if(array[maxIndex]>=0.5) {
            switch (maxIndex) {
                case 0:
                    result = "Downstairs";
                    break;
                case 1:
                    result = "Running";
                    break;
                case 2:
                    result = "Sitting";
                    break;
                case 3:
                    result = "Upstairs";
                    break;
                case 4:
                    result = "Walking";
                    break;
                default:
                    result = "Unclassified";
            }
        }else
            result = "Doing Unclasified Action";

        return result;
    }

    public static int bitReverse(int n, int bits) {
        int reversedN = n;
        int count = bits - 1;

        n >>= 1;
        while (n > 0) {
            reversedN = (reversedN << 1) | (n & 1);
            count--;
            n >>= 1;
        }

        return ((reversedN << count) & ((1 << bits) - 1));
    }

    static void fft(Complex[] buffer) {

        int bits = (int) (log(buffer.length) / log(2));
        for (int j = 1; j < buffer.length / 2; j++) {

            int swapPos = bitReverse(j, bits);
            Complex temp = buffer[j];
            buffer[j] = buffer[swapPos];
            buffer[swapPos] = temp;
        }

        for (int N = 2; N <= buffer.length; N <<= 1) {
            for (int i = 0; i < buffer.length; i += N) {
                for (int k = 0; k < N / 2; k++) {

                    int evenIndex = i + k;
                    int oddIndex = i + k + (N / 2);
                    Complex even = buffer[evenIndex];
                    Complex odd = buffer[oddIndex];

                    double term = (-2 * PI * k) / (double) N;
                    Complex exp = (new Complex(cos(term), sin(term)).mult(odd));

                    buffer[evenIndex] = even.add(exp);
                    buffer[oddIndex] = even.sub(exp);
                }
            }
        }
    }

    public double findFrequency(List<Float> list){
        Complex[] cinput = new Complex[list.size()];
        for (int i = 0; i < list.size(); i++) {
            double temp = list.get(i);
            cinput[i] = new Complex(temp, 0.0);
        }

        fft(cinput);
        double freq = cinput[1].freq;
        for (int i = 2; i<list.size()/2 + 1; i++) {
            if(freq<cinput[i].freq){
                freq = cinput[i].freq;
            }
        }

        return freq;
    }

    /*
    public void parseJSON(JSONObject jsonObject){
        try {
            actionType = jsonObject.getInt("A");   // localde tutarken gerekli olabilir. 1: Mpu 2: sıcaklık 3: Nabız
            date = jsonObject.getString("D");       // Date
            switch (actionType){
                case 1:
                    activityPrediction();
                    if(x.size()==0 || y.size() == 0 || z.size() == 0){
                        t0 = System.currentTimeMillis();
                    }
                    x.add((float)jsonObject.getDouble("X"));
                    y.add((float)jsonObject.getDouble("Y"));
                    z.add((float)jsonObject.getDouble("Z"));
                    break;
                case 2:
                    heart_rate = jsonObject.getInt("P");        // gerekli yerde gösterilecek
                    break;
                case 3:
                    temperature = jsonObject.getDouble("T");    // gerekli yerde gösterilecek
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
*/
    public List<MPUDataResponse> getMpuDataResponse() {
        return mpuDataResponse;
    }

    public void setMpuDataResponse(List<MPUDataResponse> mpuDataResponse) {
        this.mpuDataResponse = mpuDataResponse;
    }

    public ClassifiedResponse getClassifiedResponse() {
        return classifiedResponse;
    }

    public void setClassifiedResponse(ClassifiedResponse classifiedResponse) {
        this.classifiedResponse = classifiedResponse;
    }

    public void addValues(float a, float b, float c){
        x.add(a);
        y.add(b);
        z.add(c);
    }

}
