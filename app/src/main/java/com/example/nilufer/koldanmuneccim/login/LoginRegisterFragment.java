package com.example.nilufer.koldanmuneccim.login;

import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nilufer.koldanmuneccim.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nilufer on 9.05.2019.
 */

public class LoginRegisterFragment extends Fragment {

    private ViewPagerAdapter mViewPagerAdapter;

    Fragment loginFragment;
    Fragment registerFragment;

    @BindView(R.id.txt_sign_in)
    TextView txtSignIn;

    @BindView(R.id.txt_sign_up)
    TextView txtSignUp;

    @BindView(R.id.iv_tab_indicator)
    ImageView tabIndicatorImageView;

    @BindView(R.id.container)
    ViewPager mViewPager;

    float indicatorWidth;
    private int deviceWidth;
    private float startPosition, endPosition;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_register_page, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Bundle args = getArguments();
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    public void initView() {
        calculateSizes();
        tabIndicatorImageView.post(new Runnable() {
            @Override
            public void run() {
                tabIndicatorImageView.setX(startPosition);
            }
        });

        mViewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        mViewPager.setAdapter(mViewPagerAdapter);
        setAnimation();
        signInClick();
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.i("onPageScrolled", position + " - " + positionOffset + " " + positionOffsetPixels);
            }

            public void onPageSelected(int position) {
                if (position == 0)
                    signInClick();
                else
                    signUpClick();
                tabIndicatorImageView.setX(position == 0 ? startPosition : endPosition);

            }
        });
    }

    public void setAnimation() {
        mViewPager.setPageTransformer(true, new ViewPager.PageTransformer()

        {
            private static final float MIN_SCALE = 0.85f;
            private static final float MIN_ALPHA = 0.5f;

            public void transformPage(View view, float position) {
                if (mViewPager.getCurrentItem() == 0 && position <= 0) {
                    tabIndicatorImageView.setX((Math.abs(position) * (endPosition - startPosition)) + startPosition);
                } else if (mViewPager.getCurrentItem() == 1 && position >= 0) {
                    tabIndicatorImageView.setX((startPosition - endPosition) * position + endPosition);
                    Log.i("saaaa", (startPosition - endPosition) * position + endPosition + "");
                    //1 startpos
                    //0 endpos
                }
                int pageWidth = view.getWidth();
                int pageHeight = view.getHeight();

                if (position < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    view.setAlpha(0f);

                } else if (position <= 1) { // [-1,1]
                    // Modify the default slide transition to shrink the page as well
                    float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                    float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                    float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                    if (position < 0) {
                        view.setTranslationX(horzMargin - vertMargin / 2);
                    } else {
                        view.setTranslationX(-horzMargin + vertMargin / 2);
                    }

                    // Scale the page down (between MIN_SCALE and 1)
                    view.setScaleX(scaleFactor);
                    view.setScaleY(scaleFactor);

                    // Fade the page relative to its size.
                    view.setAlpha(MIN_ALPHA +
                            (scaleFactor - MIN_SCALE) /
                                    (1 - MIN_SCALE) * (1 - MIN_ALPHA));

                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    view.setAlpha(0f);
                }
            }

        });

    }


    private void calculateSizes() {
        indicatorWidth = getResources().getDimensionPixelSize(R.dimen.tab_indicator_width);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;

        startPosition = deviceWidth / 4 - indicatorWidth / 2;
        endPosition = deviceWidth * 3 / 4 - indicatorWidth / 2;
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new LoginFragment();
            } else if (position == 1) {
                return new RegisterFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @OnClick(R.id.txt_sign_in)
    public void signInClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            tabIndicatorImageView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.walkman));
        }
        mViewPager.setCurrentItem(0);
    }

    @OnClick(R.id.txt_sign_up)
    public void signUpClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            tabIndicatorImageView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.walkgirl));
        }
        mViewPager.setCurrentItem(1);
    }

}


