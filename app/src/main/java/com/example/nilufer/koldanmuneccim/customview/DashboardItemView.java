package com.example.nilufer.koldanmuneccim.customview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nilufer.koldanmuneccim.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nilufer on 13.05.2019.
 */

public class DashboardItemView extends LinearLayout {
    Context mContext;

    @BindView(R.id.img_activity)
    SquareImageView activityImage;

    @BindView(R.id.txt_activity_name)
    TextView activityName;

    public DashboardItemView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public DashboardItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public DashboardItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    public DashboardItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        init();
    }

    public void init()  {
        View rootView; // Creating an instance for View Object
        rootView = View.inflate(mContext, R.layout.activities_item,this);
        ButterKnife.bind(this,rootView);
    }

    public void setActivityName(String name)   {
        activityName.setText(name);
    }

    public void setActivityImage(int imgDrawable)  {
        activityImage.setImageDrawable(ContextCompat.getDrawable(getContext(), imgDrawable));
    }

}
