package com.example.nilufer.koldanmuneccim.model;

/**
 * Created by Nilufer on 15.05.2019.
 */

public class TemperatureDataResponse {

    Double temperature;

    public TemperatureDataResponse(Double temperature) {
        this.temperature = temperature;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }
}
