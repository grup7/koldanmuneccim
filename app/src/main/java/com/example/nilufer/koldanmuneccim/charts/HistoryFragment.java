package com.example.nilufer.koldanmuneccim.charts;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;
import com.example.nilufer.koldanmuneccim.OnGetDataListener;
import com.example.nilufer.koldanmuneccim.R;
import com.example.nilufer.koldanmuneccim.ServerDataListener;
import com.example.nilufer.koldanmuneccim.model.FbResponse;
import com.example.nilufer.koldanmuneccim.model.ServerData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment implements ServerDataListener {

    @BindView(R.id.any_chart_view)
    AnyChartView lineChartView;


    String activityName = null;
    View view;

    List<DataEntry> data = new ArrayList<>();
    DatabaseReference myRef;
    FirebaseDatabase database;
    ServerData serverDataLast;
    FbResponse serverProcessedData;

    String[] walkingData;
    String[] runningData;
    String[] stairsData;

    List<ServerData> serverDataList;


    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_history2, container, false);

        ButterKnife.bind(this, view);

        Bundle deneme = this.getArguments();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        walkingData = new String[7];
        runningData = new String[7];
        stairsData = new String[7];
        serverDataList = new ArrayList<>();

        if (deneme != null) {

            activityName = deneme.getString("activity");

        }

        takeDataFromDb1();

        // hareketlere göre gelen veriyi DATA arrayindeki value değerine verirsen hallolur her şey


        return view;
    }

    private void drawChart(String xAxisTitle) {

        AnyChartView anyChartView = view.findViewById(R.id.any_chart_view);
        ProgressBar progressBar = view.findViewById(R.id.progress_bar);
        anyChartView.setProgressBar(progressBar);


        Cartesian cartesian = AnyChart.column();

        Column column = cartesian.column(data);

        column.tooltip()
                .titleFormat("{%X}")
                .position(Position.CENTER_BOTTOM)
                .anchor(Anchor.CENTER_BOTTOM)
                .offsetX(0d)
                .offsetY(5d)
                .format("{%Value}{groupsSeparator: }");

        cartesian.animation(true);
        String title = "18 - 24 Mayıs Hareket Grafiği";
        cartesian.title(title);

        cartesian.yScale().minimum(0d);

        cartesian.yAxis(0).labels().format("{%Value}{groupsSeparator: }");

        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);
        cartesian.interactivity().hoverMode(HoverMode.BY_X);

        cartesian.xAxis(0).title("Mayıs 2019");
        cartesian.yAxis(0).title(xAxisTitle);

        cartesian.credits().enabled(false);
        cartesian.credits().text("");
        cartesian.credits().alt("");

        anyChartView.setChart(cartesian);

    }

    private void takeDataFromDb1() {

        Query databaseQuery = myRef.child("ServerData");
        getData(databaseQuery, new OnGetDataListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(DataSnapshot data) {
                for (DataSnapshot eachDataSnapShot : data.getChildren()) {
                    serverDataLast = eachDataSnapShot.getValue(ServerData.class);
                    serverDataList.add(serverDataLast);
                }

                if (serverDataList != null) {
                    getServerData(serverDataList);
                }

            }

            @Override
            public void onFailed(DatabaseError databaseError) {
            }
        });
    }


    private void getData(Query query, final OnGetDataListener listener) {
        listener.onStart();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
                Log.d("", "Error trying to get classified ad for update " +
                        "" + databaseError);
            }
        });
    }


    @Override
    public void getServerData(List<ServerData> serverDataList1) {
        ServerData serverData;

        for (int i = 0; i < 7; ++i) {
            serverData = serverDataList1.get(i);
            if (serverData.getDate().equals("2019-5-18")) {
                walkingData[0] = serverData.getWalkingData();
                runningData[0] = serverData.getRunningData();
                stairsData[0] = serverData.getUpStairsData();

            } else if (serverData.getDate().equals("2019-5-19")) {
                walkingData[1] = serverData.getWalkingData();
                runningData[1] = serverData.getRunningData();
                stairsData[1] = serverData.getUpStairsData();
            } else if (serverData.getDate().equals("2019-5-20")) {
                walkingData[2] = serverData.getWalkingData();
                runningData[2] = serverData.getRunningData();
                stairsData[2] = serverData.getUpStairsData();
            } else if (serverData.getDate().equals("2019-5-21")) {
                walkingData[3] = serverData.getWalkingData();
                runningData[3] = serverData.getRunningData();
                stairsData[3] = serverData.getUpStairsData();
            } else if (serverData.getDate().equals("2019-5-22")) {
                walkingData[4] = serverData.getWalkingData();
                runningData[4] = serverData.getRunningData();
                stairsData[4] = serverData.getUpStairsData();
            } else if (serverData.getDate().equals("2019-5-23")) {
                walkingData[5] = serverData.getWalkingData();
                runningData[5] = serverData.getRunningData();
                stairsData[5] = serverData.getUpStairsData();
            } else if (serverData.getDate().equals("2019-5-24")) {
                walkingData[6] = serverData.getWalkingData();
                runningData[6] = serverData.getRunningData();
                stairsData[6] = serverData.getUpStairsData();
            }
        }


        String yaxis = "Saat";

        if (activityName != null && activityName.equals("Stairs")) {

            data.clear();

            data.add(new ValueDataEntry("18", Integer.parseInt(stairsData[0])));
            data.add(new ValueDataEntry("19", Integer.parseInt(stairsData[1])));
            data.add(new ValueDataEntry("20", Integer.parseInt(stairsData[2])));
            data.add(new ValueDataEntry("21", Integer.parseInt(stairsData[3])));
            data.add(new ValueDataEntry("22", Integer.parseInt(stairsData[4])));
            data.add(new ValueDataEntry("23", Integer.parseInt(stairsData[5])));
            data.add(new ValueDataEntry("24", Integer.parseInt(stairsData[6])));

            yaxis = "Kat";


        } else if (activityName != null && activityName.equals("Pulse")) {

            data.clear();

            data.add(new ValueDataEntry("18", 102));
            data.add(new ValueDataEntry("19", 98));
            data.add(new ValueDataEntry("20", 100));
            data.add(new ValueDataEntry("21", 95));
            data.add(new ValueDataEntry("22", 97));
            data.add(new ValueDataEntry("23", 110));
            data.add(new ValueDataEntry("24", 90));


            yaxis = "BPM";


        } else if (activityName != null && activityName.equals("Temperature")) {

            data.clear();

            data.add(new ValueDataEntry("18", 37));
            data.add(new ValueDataEntry("19", 36));
            data.add(new ValueDataEntry("20", 37));
            data.add(new ValueDataEntry("21", 38));
            data.add(new ValueDataEntry("22", 38));
            data.add(new ValueDataEntry("23", 37));
            data.add(new ValueDataEntry("24", 37));


            yaxis = "Derece";


        } else {


            int percentage = 25;

            switch (activityName) {
                case "Walking":


                    data.clear();

                    data.add(new ValueDataEntry("18", Integer.parseInt(walkingData[0])));
                    data.add(new ValueDataEntry("19", Integer.parseInt(walkingData[1])));
                    data.add(new ValueDataEntry("20", Integer.parseInt(walkingData[2])));
                    data.add(new ValueDataEntry("21", Integer.parseInt(walkingData[3])));
                    data.add(new ValueDataEntry("22", Integer.parseInt(walkingData[4])));
                    data.add(new ValueDataEntry("23", Integer.parseInt(walkingData[5])));
                    data.add(new ValueDataEntry("24", Integer.parseInt(walkingData[6])));

                    yaxis = "Adım";

                    break;
                case "Running":


                    data.clear();

                    data.add(new ValueDataEntry("18", Float.valueOf(runningData[0])));
                    data.add(new ValueDataEntry("19", Float.valueOf(runningData[1])));
                    data.add(new ValueDataEntry("20", Float.valueOf(runningData[2])));
                    data.add(new ValueDataEntry("21", Float.valueOf(runningData[3])));
                    data.add(new ValueDataEntry("22", Float.valueOf(runningData[4])));
                    data.add(new ValueDataEntry("23", Float.valueOf(runningData[5])));
                    data.add(new ValueDataEntry("24", Float.valueOf(runningData[6])));

                    yaxis = "Metre";

                    break;
            }


        }


        drawChart(yaxis);

    }
}
