package com.example.nilufer.koldanmuneccim.model;

import java.util.List;

/**
 * Created by Nilufer on 17.05.2019.
 */

public class PulseListObj {
    List<Integer> pulseDataList;


    public PulseListObj(List<Integer> pulseDataList) {
        this.pulseDataList = pulseDataList;
    }

    public PulseListObj() {
    }

    public List<Integer> getPulseDataList() {
        return pulseDataList;
    }

    public void setPulseDataList(List<Integer> pulseDataList) {
        this.pulseDataList = pulseDataList;
    }
}
