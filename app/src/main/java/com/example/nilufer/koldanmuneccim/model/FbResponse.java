package com.example.nilufer.koldanmuneccim.model;

import java.util.List;

/**
 * Created by Nilufer on 17.05.2019.
 */

public class FbResponse {
    List<ServerData> serverDataList;

    public FbResponse() {
    }

    public FbResponse(List<ServerData> serverDataList) {
        this.serverDataList = serverDataList;
    }

    public List<ServerData> getServerDataList() {
        return serverDataList;
    }

    public void setServerDataList(List<ServerData> serverDataList) {
        this.serverDataList = serverDataList;
    }
}
