package com.example.nilufer.koldanmuneccim.model;

import java.util.List;

/**
 * Created by Nilufer on 17.05.2019.
 */

public class TemperatureDataList {
    List<Double> temperatureList;

    public TemperatureDataList(List<Double> temperatureList) {
        this.temperatureList = temperatureList;
    }

    public TemperatureDataList() {
    }

    public List<Double> getTemperatureList() {
        return temperatureList;
    }

    public void setTemperatureList(List<Double> temperatureList) {
        this.temperatureList = temperatureList;
    }
}
