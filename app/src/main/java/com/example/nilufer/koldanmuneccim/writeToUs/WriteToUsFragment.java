package com.example.nilufer.koldanmuneccim.writeToUs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nilufer.koldanmuneccim.R;

import butterknife.ButterKnife;

public class WriteToUsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.view_write_to_us_fragment, container, false);
        Bundle args = getArguments();
        ButterKnife.bind(this, view);


        return view;



    }


}
