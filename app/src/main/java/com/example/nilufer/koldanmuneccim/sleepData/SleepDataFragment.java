package com.example.nilufer.koldanmuneccim.sleepData;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nilufer.koldanmuneccim.R;

public class SleepDataFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //CHANGE THIS
        return inflater.inflate(R.layout.sleep_data_display, container, false);
    }
}
