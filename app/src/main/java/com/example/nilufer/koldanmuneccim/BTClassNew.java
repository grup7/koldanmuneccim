package com.example.nilufer.koldanmuneccim;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.nilufer.koldanmuneccim.bluetooth.DeviceListAdapter;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nilufer on 17.05.2019.
 */

public class BTClassNew extends Fragment {

    BluetoothAdapter mBluetoothAdapter;
    private ListView btListView;

    private ListView btListViewPaired;

    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();
    public DeviceListAdapter mDeviceListAdapter;
    public ArrayList<BluetoothDevice> mBTDevicesPaired = new ArrayList<>();

 //   @BindView(R.id.listViewBT)
 //   ListView btListview;

    View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_connect_to_bluetooth, container, false);
        ButterKnife.bind(this, view);

        initView();

        return view;
    }

    public void initView() {
        btListView.setClickable(true);
        btListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println(mBTDevices.get(i).getName());
                if (mBTDevices.get(i).getName().equalsIgnoreCase("Grup7")) {
                    mBluetoothAdapter.cancelDiscovery();
                    if (!mBTDevicesPaired.contains(mBTDevices.get(i))) {
                        try {


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {


                    }
                } else {


                }
            }
        });

    //    btListViewPaired = view.findViewById(R.id.listViewBTPaired);
        mBTDevices = new ArrayList<>();
        mBTDevicesPaired = new ArrayList<>();
    }

  //  @OnClick(R.id.listViewBT)
    public void listViewBT() {

        }
    }

