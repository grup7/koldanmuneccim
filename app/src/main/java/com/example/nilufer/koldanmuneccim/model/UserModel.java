package com.example.nilufer.koldanmuneccim.model;

/**
 * Created by Nilufer on 13.05.2019.
 */

public class UserModel {
    String nameSurname;
    String phoneNumber;
    String birthdate;
    String gender;
    String height;
    String weight;
    String targetWalking;
    String targetRunning;
    String imgUri;
    ServerData serverData;

    public UserModel(String nameSurname, String phoneNumber, String birthdate, String gender, String height, String weight,
                     String targetWalking, String targetRunning, String imgUri, ServerData serverData) {
        this.nameSurname = nameSurname;
        this.phoneNumber = phoneNumber;
        this.birthdate = birthdate;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
        this.imgUri = imgUri;
        this.targetRunning = targetRunning;
        this.targetWalking = targetWalking;
        this.serverData = serverData;
    }

    public UserModel() {
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTargetWalking() {
        return targetWalking;
    }

    public void setTargetWalking(String targetWalking) {
        this.targetWalking = targetWalking;
    }

    public String getTargetRunning() {
        return targetRunning;
    }

    public void setTargetRunning(String targetRunning) {
        this.targetRunning = targetRunning;
    }

    public String getImgUri() {
        return imgUri;
    }

    public void setImgUri(String imgUri) {
        this.imgUri = imgUri;
    }

    public ServerData getServerData() {
        return serverData;
    }

    public void setServerData(ServerData serverData) {
        this.serverData = serverData;
    }
}
