package com.example.nilufer.koldanmuneccim;

/**
 * Created by Nilufer on 24.05.2019.
 */

public interface FbFlagListener {

    public void listenFlagChanges(String flagVal);
}
