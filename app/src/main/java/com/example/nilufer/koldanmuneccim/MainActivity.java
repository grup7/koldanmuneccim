package com.example.nilufer.koldanmuneccim;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.appus.splash.Splash;
import com.example.nilufer.koldanmuneccim.login.LoginRegisterFragment;
import com.example.nilufer.koldanmuneccim.model.ClassifiedResponse;
import com.example.nilufer.koldanmuneccim.model.MPUDataResponse;
import com.example.nilufer.koldanmuneccim.model.SendServerDataModel;
import com.example.nilufer.koldanmuneccim.retrofit.APIClient;
import com.example.nilufer.koldanmuneccim.retrofit.ServiceCallInterface;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements FbFlagListener {

    SharedPreferences.Editor editor;
    int walkingData = 0;
    double runningData = 0;
    int upstairsData = 0;
    int downstairsData = 0;
    double temperature = 0;
    int pulse = 0;
    String flagVal = "-1";
    int nil = -1;
    String date;
    boolean come = false;

    private final int REQUEST_ENABLE_BT = 0;
    DatabaseReference myRef;
    FirebaseDatabase database;
    public Classifier classifier;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    SendServerDataModel sendServerDataModel;
    ServiceCallInterface serviceCallInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        classifier = new Classifier(getApplicationContext());
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        serviceCallInterface = APIClient.getClient().create(ServiceCallInterface.class);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = df.format(Calendar.getInstance().getTime());

        Splash.Builder splash = new Splash.Builder(this, getSupportActionBar());
        splash.setBackgroundColor(getResources().getColor(R.color.generalBlue));
        splash.setSplashImage(getResources().getDrawable(R.drawable.footstep));
        splash.perform();
        checkBluetooth();


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, new LoginRegisterFragment());
        transaction.disallowAddToBackStack();
        transaction.commitAllowingStateLoss();


        editor = getSharedPreferences("shared", MODE_PRIVATE).edit();
        editor.putBoolean("isLoginAfter", false);
        editor.apply();


  /*      DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = df.format(Calendar.getInstance().getTime());
        sendServerDataModel = new SendServerDataModel(13, date, 8, 38);
        List<SendServerDataModel> sendServerDataModelList = new ArrayList<>();
        sendServerDataModelList.add(sendServerDataModel);
        Call<Void> call = serviceCallInterface.writeServerData(sendServerDataModelList);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("", "pulse başarılı");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("", "pulse başarısız");
            }
        });

*/

        final ExecutorService es = Executors.newCachedThreadPool();
        ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        ses.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                es.submit(new Runnable() {
                    @Override
                    public void run() {
                        if (come == true)
                            writeDataOnServer();
                        come=false;
                    }
                });
            }
        }, 0, 60, TimeUnit.MINUTES);

    }

    public void writeDataOnServer() {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = df.format(Calendar.getInstance().getTime());
        sendServerDataModel = new SendServerDataModel(13, date, 1, walkingData);
        List<SendServerDataModel> sendServerDataModelList = new ArrayList<>();
        sendServerDataModelList.add(sendServerDataModel);
        Call<Void> call = serviceCallInterface.writeServerData(sendServerDataModelList);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("", "walk başarılı");
                runCall();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("", "walk başarısız");
            }
        });
    }

    public void runCall() {
        sendServerDataModel = new SendServerDataModel(13, date, 2, (int) runningData);
        List<SendServerDataModel> sendServerDataModelList = new ArrayList<>();
        sendServerDataModelList.add(sendServerDataModel);
        Call<Void> call = serviceCallInterface.writeServerData(sendServerDataModelList);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("", "run başarılı");
                downstCall();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("", "run başarısız");
            }
        });

    }

    public void downstCall() {

        sendServerDataModel = new SendServerDataModel(13, date, 3, downstairsData);
        List<SendServerDataModel> sendServerDataModelList = new ArrayList<>();
        sendServerDataModelList.add(sendServerDataModel);
        Call<Void> call = serviceCallInterface.writeServerData(sendServerDataModelList);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("", "down başarılı");
                upstCal();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("", "down başarısız");
            }
        });

    }

    public void upstCal() {

        sendServerDataModel = new SendServerDataModel(13, date, 4, upstairsData);
        List<SendServerDataModel> sendServerDataModelList = new ArrayList<>();
        sendServerDataModelList.add(sendServerDataModel);
        Call<Void> call = serviceCallInterface.writeServerData(sendServerDataModelList);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("", "down başarılı");
                sleepCall();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("", "down başarısız");
            }
        });

    }

    public void sleepCall() {

        sendServerDataModel = new SendServerDataModel(13, date, 5, 7);
        List<SendServerDataModel> sendServerDataModelList = new ArrayList<>();
        sendServerDataModelList.add(sendServerDataModel);
        Call<Void> call = serviceCallInterface.writeServerData(sendServerDataModelList);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("", "sleep başarılı");
                standCall();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("", "sleep başarısız");
            }
        });

    }

    public void standCall() {

        sendServerDataModel = new SendServerDataModel(13, date, 3, 6);
        List<SendServerDataModel> sendServerDataModelList = new ArrayList<>();
        sendServerDataModelList.add(sendServerDataModel);
        Call<Void> call = serviceCallInterface.writeServerData(sendServerDataModelList);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("", "stand başarılı");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("", "stand başarısız");
            }
        });

    }

    @Override
    public void onBackPressed() {
        SharedPreferences prefs = getSharedPreferences("shared", MODE_PRIVATE);
        Boolean isLoginAfter = prefs.getBoolean("isLoginAfter", Boolean.parseBoolean(null));

        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            if (isLoginAfter == false)
                super.onBackPressed();
            else {
                createDialog(this);
            }
        } else
            super.onBackPressed();

    }

    public void createDialog(final Context context) {
        new FancyGifDialog.Builder(this)
                .setMessage("Uygulamadan çıkış yapmak istediğinize emin misiniz?")
                .setNegativeBtnText("Vazgeç")
                .setPositiveBtnBackground("#000000")
                .setPositiveBtnText("Çıkış Yap")
                .setNegativeBtnBackground("#000000")
                .setGifResource(R.drawable.giphy)   //Pass your Gif here
                .isCancellable(true)
                .OnPositiveClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                        finish();
                    }
                })
                .OnNegativeClicked(new FancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                    }
                })
                .build();
    }


    public void checkBluetooth() {
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "This device doesn't have a bluetooth", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!mBluetoothAdapter.isEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_ENABLE_BT);
        } else {
            // bt enabled check for connection
            checkForConnection();
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK) {
                    checkForConnection();
                }
                if (resultCode == RESULT_CANCELED) {
                    //
                }

        }
    }

    @TargetApi(23)
    private void checkBTPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck;
            permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION") + this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        } else {
            //Log.d(TAG, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }

    boolean findBT() {
        System.out.println("Finding bt");
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        boolean found = false;
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equalsIgnoreCase("grup7")) {
                    found = true;
                    mmDevice = device;
                    break;
                }
            }
        }
        if (found)
            return true;
        else {
            showAlertDialog();
            return false;
        }
    }

    void openBT() throws IOException {
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);

        if (!mmSocket.isConnected()) {
            mmSocket.connect();
            System.out.println("Yeni bağlantı kuruluyor");
        } else {
            System.out.println("Zaten bağlantı var");
        }
        mmOutputStream = mmSocket.getOutputStream();
        mmInputStream = mmSocket.getInputStream();


        Toast.makeText(this, "Bluetooth Bağlantısı Kuruldu", Toast.LENGTH_SHORT).show();
        beginListenForData();
    }

    void checkForConnection() {
        try {
            boolean b = findBT();
            if (b == true)
                openBT();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Connection check finished.");
    }

    public void closeBT() throws IOException {

        stopWorker = true;
        mmOutputStream.close();
        mmInputStream.close();
        mmSocket.close();
        mmDevice = null;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            closeBT();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void beginListenForData() {
        System.out.println("Data listening");
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        DatabaseReference myRef3 = FirebaseDatabase.getInstance().getReference().child("BluetoothData").child("flag");
        myRef3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null)
                    flagVal = dataSnapshot.getValue(String.class);
                listenFlagChanges(flagVal);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable() {
            public void run() {
                while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                    try {

                        int bytesAvailable = mmInputStream.available();
                        if (bytesAvailable > 0) {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for (int i = 0; i < bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                if (b == delimiter) {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable() {
                                        public void run() {
                                            try {
                                                Log.d("BTREADER", "" + "Hello");
                                                System.out.println("Data recieved");
                                                JSONObject reader = new JSONObject(data);
                                                parseJSON(reader);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                } else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    } catch (IOException ex) {
                        stopWorker = true;
                        System.out.println("worker stopped");
                        Log.d("BTSTATE", "Stoped");
                    }

                }
            }
        });

        workerThread.start();
    }

    public void parseJSON(JSONObject jsonObject) {
        if (jsonObject == null)
            return;
        try {
            int actionType;
            String date = "";
            actionType = jsonObject.getInt("A");
            date = jsonObject.getString("D");
            switch (actionType) {
                case 1:
                    float x, y, z;
                    x = ((float) jsonObject.getDouble("X"));
                    y = ((float) jsonObject.getDouble("Y"));
                    z = ((float) jsonObject.getDouble("Z"));
                    Log.i("", String.valueOf(x));
                    classifier.addValues(x, y, z);
                    classifier.activityPrediction();
                    ClassifiedResponse cR = classifier.getClassifiedResponse();

                    if (cR != null) {
                        downstairsData = cR.getDownStairsNumber();
                        upstairsData = cR.getUpStairsNumber();
                        if (flagVal.equals("1")) {
                            if (nil == -1)
                                cR.setRunning(0);
                            nil = 1;
                            runningData = cR.getRunning();
                        }
                        walkingData = cR.getWalkingData();
                        DatabaseReference dbRef = database.getReference("BluetoothData").child("deneme");
                        dbRef.setValue(String.valueOf(walkingData));
                        System.out.println("Step counter:" + cR.getWalkingData());
                        if (flagVal.equals("1")) {
                            DatabaseReference dbRef2 = database.getReference("BluetoothData").child("Running");
                            DecimalFormat df2 = new DecimalFormat("#.##");
                            dbRef2.setValue(df2.format(runningData));


                        }

                        DatabaseReference dbRef3 = database.getReference("BluetoothData").child("Stairs");
                        dbRef3.setValue(String.valueOf(upstairsData));
                    }

                    break;
                case 2:
                    pulse = jsonObject.getInt("P");
                    DatabaseReference dbRef4 = database.getReference("BluetoothData").child("Pulse");
                    dbRef4.setValue(String.valueOf(pulse));
                    pulseCall();
                    Log.i("", String.valueOf(jsonObject.getInt("P")));
                    break;
                case 3:
                    temperature = (jsonObject.getDouble("T"));    // gerekli yerde gösterilecek
                    DatabaseReference dbRef5 = database.getReference("BluetoothData").child("Temperature");
                    dbRef5.setValue(String.valueOf(temperature));
                    temperatureCall();
                    Log.i("", String.valueOf(jsonObject.getInt("T")));
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void pulseCall() {
        if (pulse != 0) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = df.format(Calendar.getInstance().getTime());
            sendServerDataModel = new SendServerDataModel(13, date, 7, pulse);
            List<SendServerDataModel> sendServerDataModelList = new ArrayList<>();
            sendServerDataModelList.add(sendServerDataModel);
            Call<Void> call = serviceCallInterface.writeServerData(sendServerDataModelList);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Log.i("", "pulse başarılı");
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.i("", "pulse başarısız");
                }
            });
        }

    }

    public void temperatureCall() {

        if (temperature != 0) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = df.format(Calendar.getInstance().getTime());
            sendServerDataModel = new SendServerDataModel(13, date, 8, (int) temperature);
            List<SendServerDataModel> sendServerDataModelList = new ArrayList<>();
            sendServerDataModelList.add(sendServerDataModel);
            Call<Void> call = serviceCallInterface.writeServerData(sendServerDataModelList);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Log.i("", "pulse başarılı");
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.i("", "pulse başarısız");
                }
            });
        }

    }

    public void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle("Caution");
        alertDialog.setMessage("Lütfen cihazınızı bluetooht ayarlarından 'grup7' cihazı ile eşleştirin");
        alertDialog.setNeutralButton("Tamam",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void listenFlagChanges(String flagVal) {
        this.flagVal = flagVal;
        if (flagVal.equals("1"))
            nil = -1;
    }
}
