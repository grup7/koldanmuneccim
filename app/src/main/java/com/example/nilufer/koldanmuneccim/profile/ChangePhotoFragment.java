package com.example.nilufer.koldanmuneccim.profile;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.nilufer.koldanmuneccim.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePhotoFragment extends Fragment {
/*    @BindView(R.id.ivPhoto)
    ImageView ivPhoto;

    @BindView(R.id.btnSelectPhoto)
    Button btnSelect;

    @BindView(R.id.btnSavePhoto)
    Button btnSave;

    private static final int IMAGE_REQUEST = 123;
    private ProgressDialog progressDialog;
    private ChangePhotoPresenter changePhotoPresenter;
    private Uri filePath;
    private FirebaseStorage firebaseStorage;
    private FirebaseAuth mFirebaseAuthentication;
    private FirebaseUser firebaseUser;

    public ChangePhotoFragment() {
        // Required empty public constructor
        changePhotoPresenter = new ChangePhotoPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_change_photo, container, false);

        firebaseStorage = FirebaseStorage.getInstance();
        mFirebaseAuthentication = FirebaseAuth.getInstance();
        ButterKnife.bind(this,view);
        showPhoto();
        return view;

    }

    private void showPhoto() {
        showProgressDialog();

        firebaseUser = mFirebaseAuthentication.getCurrentUser();
        String childName = "userProfilePhoto";
        if (firebaseUser != null) {
            childName= "userProfilePhoto/" + firebaseUser.getUid();
        }

        StorageReference storageReference = firebaseStorage.getReference().child(childName);
        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {

            @Override
            public void onSuccess(Uri uri) {

                progressDialog.dismiss();

                Glide.with(getContext()).load(uri).asBitmap().centerCrop().into(new SimpleTarget<Bitmap>(200,200) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        ivPhoto.setImageBitmap(resource);
                    }
                });

                //Picasso.with(getContext()).load(uri).fit().centerCrop().into(ivPhoto);
                //Toast.makeText(getContext(), "Fotoğraf Getirildi.", Toast.LENGTH_SHORT).show();

            }

        }).addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
               // Toast.makeText(getContext(), "Fotoğraf Getirilemedi.", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @OnClick (R.id.btnSelectPhoto)
    public void selectPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Fotoğraf Seçiniz"), IMAGE_REQUEST);
    }

    @OnClick (R.id.btnSavePhoto)
    public void savePhoto() {
        changePhotoPresenter.setFilePath(filePath);
        changePhotoPresenter.savePhoto();
    }
    @OnClick (R.id.btnBack)
    public void back(){
        Fragment fragment = new EditProfileFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();
    }
    private void showProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Yükleniyor");
        progressDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Glide.with(getContext()).load(filePath).asBitmap().centerCrop().into(new SimpleTarget<Bitmap>(200,200) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        ivPhoto.setImageBitmap(resource);
                    }
                });
                //Picasso.with(getContext()).load(filePath).fit().centerCrop().into(ivPhoto);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
*/
}
