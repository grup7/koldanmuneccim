import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.example.nilufer.koldanmuneccim.MainActivity;

/**
 * Created by Nilufer on 17.05.2019.
 */

public class BluetoothNotifyService extends Service {
    public BluetoothNotifyService() {
    }

    @Override
    public void onCreate() {
        new Thread(new Runnable() {  // Yeni bir Thread (iş parcacığı) oluşturuyorum.
            @Override
            public void run() { // Thread'ım başladığında bitmemesi için while
                // ile sonsuz döngüye soktum. senaryo gereği
                while (1 == 1) {
                    try {
                        Thread.sleep(15000); // Her döngümde Thread'ımı 15000 ms uyutuyorum.
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }
            }
        }).start();  // burada Thread'ımı başlatıyorum.
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");


    }
}